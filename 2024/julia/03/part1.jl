using Test

function solve(filename)
    data = read(filename, String)
    result = 0
    for m in findall(r"mul\(\d+\,\d+\)", data)
        n1, n2 = eachmatch(r"\d+", data[m])
        result += parse(Int64, String(n1.match)) * parse(Int, String(n2.match))
    end
    result
end

if abspath(PROGRAM_FILE) == @__FILE__
    if length(ARGS) == 1 && ARGS[1] == "test"
        @testset "Part 1 test" begin
            @test solve("inputs/input-day03-00-test1.txt") == 161
        end
    elseif length(ARGS) == 0
        file_name = "inputs/input-day03-01.txt"
        if !ispath(file_name)
            error("File $(file_name) does not exist")
        end
        @info("Part 1: $(solve(file_name))")
    end
end
