using Test

function solve(filename)
    data = read(filename, String)
    r_mul = r"mul\(\d+\,\d+\)"
    r_do = r"do\(\)"
    r_dont = r"don\'t\(\)"
    result = 0
    enabled = true
    s = 1
    while true
        ix = findnext(r_mul, data, s)
        if isnothing(ix)
            break
        end
        next_s = first(ix) + 1

        if !enabled
            next_do = findnext(r_do, data, s)
            if isnothing(next_do)
                break
            else
                next_s = first(next_do) + 1
                enabled = !enabled
            end
        else
            n1, n2 = eachmatch(r"\d+", data[ix])
            result += parse(Int64, String(n1.match)) * parse(Int, String(n2.match))
            next_ix = findnext(r_mul, data, next_s)
            next_dont = findnext(r_dont, data, s)
            if !isnothing(next_ix) && !isnothing(next_dont)
                next_dont = first(next_dont)
                if first(next_dont) < first(next_ix)
                    enabled = !enabled
                end
            end
        end
        s = next_s
    end
    result
end

if abspath(PROGRAM_FILE) == @__FILE__
    if length(ARGS) == 1 && ARGS[1] == "test"
        @testset "Part 2 test" begin
            @test solve("inputs/input-day03-00-test2.txt") == 48
        end
    elseif length(ARGS) == 0
        file_name = "inputs/input-day03-01.txt"
        if !ispath(file_name)
            error("File $(file_name) does not exist")
        end
        @info("Part 2: $(solve(file_name))")
    end
end
