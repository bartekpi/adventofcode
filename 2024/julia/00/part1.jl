using Test

function solve(filename)

end

if abspath(PROGRAM_FILE) == @__FILE__
    if length(ARGS) == 1 && ARGS[1] == "test"
        @testset "Part 1 test" begin
            @test solve("inputs/input_file") == 0
        end
    elseif length(ARGS) == 0
        file_name = "inputs/input_file"
        if !ispath(file_name)
            error("File $(file_name) does not exist")
        end
        @info("Part 1: $(solve(file_name))")
    end
end
