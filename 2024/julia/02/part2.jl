using Test

function solve(filename)
    safe_lines = 0
    for line in readlines(filename)
        numbers = map(x -> parse(Int64, x), split(line, " "))
        diffs = [numbers[i] - numbers[i-1] for i in eachindex(numbers)[2:end]]
        min_diff, max_diff = minimum(abs.(diffs)), maximum(abs.(diffs))
        check_diffs = (min_diff >= 1) && (max_diff <= 3)
        check_asc = all(diffs .> 0)
        check_desc = all(diffs .< 0)
        if check_diffs && (check_asc || check_desc)
            safe_lines += 1
            continue
        else
            for i in eachindex(numbers)
                numbers_2 = [x for (ix, x) in enumerate(numbers) if ix != i]
                diffs = [numbers_2[i] - numbers_2[i-1] for i in eachindex(numbers_2)[2:end]]
                min_diff, max_diff = minimum(abs.(diffs)), maximum(abs.(diffs))
                check_diffs = (min_diff >= 1) && (max_diff <= 3)
                check_asc = all(diffs .> 0)
                check_desc = all(diffs .< 0)
                if check_diffs && (check_asc || check_desc)
                    safe_lines += 1
                    break
                end

            end
        end
    end

    safe_lines

end

if abspath(PROGRAM_FILE) == @__FILE__
    if length(ARGS) == 1 && ARGS[1] == "test"
        @testset "Part 2 test" begin
            @test solve("inputs/input-day02-00-test1.txt") == 4
        end
    elseif length(ARGS) == 0
        file_name = "inputs/input-day02-01.txt"
        if !ispath(file_name)
            error("File $(file_name) does not exist")
        end
        @info("Part 2: $(solve(file_name))")
    end
end
