using Test

function solve(filename)
    lines = readlines(filename)
    list1 = []
    list2 = Dict()
    for line in lines
        line_split = line |> split
        push!(list1, parse(Int64, line_split[1]))
        v2 = parse(Int64, line_split[2])
        if v2 in keys(list2)
            list2[v2] += 1
        else
            list2[v2] = 1
        end

    end
    sort!(list1)
    result = 0
    for x in list1
        if x in keys(list2)
            result += x * list2[x]
        end
    end
    result
end

if abspath(PROGRAM_FILE) == @__FILE__
    if length(ARGS) == 1 && ARGS[1] == "test"
        @testset "Part 2 test" begin
            @test solve("inputs/input-day01-00-test1.txt") == 31
        end
    elseif length(ARGS) == 0
        file_name = "inputs/input-day01-01.txt"
        if !ispath(file_name)
            error("File $(file_name) does not exist")
        end
        @info("Part 2: $(solve(file_name))")
    end
end
