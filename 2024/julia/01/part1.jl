using Test

function solve(filename)
    lines = readlines(filename)
    list1, list2 = [], []
    for line in lines
        line_split = line |> split
        push!(list1, parse(Int64, line_split[1]))
        push!(list2, parse(Int64, line_split[2]))

    end
    sort!(list1)
    sort!(list2)
    result = 0
    for (x, y) in zip(list1, list2)
        result += abs(x - y)
    end
    result
end

if abspath(PROGRAM_FILE) == @__FILE__
    if length(ARGS) == 1 && ARGS[1] == "test"
        @testset "Part 1 test" begin
            @test solve("inputs/input-day01-00-test1.txt") == 11
        end
    elseif length(ARGS) == 0
        file_name = "inputs/input-day01-01.txt"
        if !ispath(file_name)
            error("File $(file_name) does not exist")
        end
        @info("Part 1: $(solve(file_name))")
    end
end
