use std::collections::HashMap;

#[allow(unused_imports)]
use log::*;

#[allow(unused_imports)]
use crate::utils;

#[allow(dead_code, unused_variables)]
pub fn part1(filename: &str) -> usize {
    let result = utils::read_input(filename)
        .map(|lines| {
            let nums: Vec<Vec<i32>> = lines
                .iter()
                .map(|line| {
                    line.split_whitespace()
                        .map(|s| s.parse().unwrap_or(0))
                        .collect()
                })
                .collect();

            let mut l1: Vec<i32> = nums.iter().map(|v| v[0]).collect();
            l1.sort();
            let mut l2: Vec<i32> = nums.iter().map(|v| v[1]).collect();
            l2.sort();

            let s: i32 = l1
                .iter()
                .zip(l2.iter())
                .map(|(v1, v2)| (v1 - v2).abs())
                .sum();

            s as usize
        })
        .unwrap_or(0);

    result
}

#[allow(dead_code, unused_variables)]
pub fn part2(filename: &str) -> usize {
    let result = utils::read_input(filename)
        .map(|lines| {
            let nums: Vec<Vec<i32>> = lines
                .iter()
                .map(|line| {
                    line.split_whitespace()
                        .map(|s| s.parse().unwrap_or(0))
                        .collect()
                })
                .collect();

            let mut l1: Vec<i32> = nums.iter().map(|v| v[0]).collect();
            l1.sort();
            let l2: Vec<i32> = nums.iter().map(|v| v[1]).collect();

            let mut d: HashMap<i32, i32> = HashMap::new();
            for i in l2.iter() {
                d.entry(*i).and_modify(|v| *v += 1).or_insert(1);
            }

            let s: i32 = l1.iter().map(|&v| v * *d.entry(v).or_insert(0)).sum();

            s as usize
        })
        .unwrap_or(0);

    result
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::utils::test_utils::init_logger;

    #[test]
    fn test1() {
        init_logger();
        assert_eq!(part1("assets/input-day01-00-test1.txt"), 11);
    }

    #[test]
    fn test2() {
        init_logger();
        assert_eq!(part2("assets/input-day01-00-test1.txt"), 31);
    }
}
