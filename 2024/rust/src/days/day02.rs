#[allow(unused_imports)]
use log::*;

#[allow(unused_imports)]
use crate::utils;

#[allow(dead_code, unused_variables)]
pub fn part1(filename: &str) -> usize {
    let result = utils::read_input(filename)
        .map(|lines| {
            lines
                .iter()
                .map(|line| {
                    let numbers: Vec<i32> = line
                        .split_whitespace()
                        .map(|s| s.parse().unwrap_or(0))
                        .collect();

                    let diffs: Vec<i32> = numbers.windows(2).map(|w| w[1] - w[0]).collect();
                    let min_diff = diffs.iter().map(|x| x.abs()).min().unwrap();
                    let max_diff = diffs.iter().map(|x| x.abs()).max().unwrap();
                    let check_diffs = (min_diff >= 1) && (max_diff <= 3);
                    let check_asc = diffs
                        .iter()
                        .map(|&x| x > 0)
                        .collect::<Vec<bool>>()
                        .iter()
                        .all(|&x| x);
                    let check_desc = diffs
                        .iter()
                        .map(|&x| x < 0)
                        .collect::<Vec<bool>>()
                        .iter()
                        .all(|&x| x);

                    let is_safe = check_diffs && (check_asc || check_desc);
                    is_safe as usize
                })
                .sum()
        })
        .unwrap_or(0);
    result
}

#[allow(dead_code, unused_variables)]
pub fn part2(filename: &str) -> usize {
    let result = utils::read_input(filename)
        .map(|lines| {
            lines
                .iter()
                .map(|line| {
                    let numbers: Vec<i32> = line
                        .split_whitespace()
                        .map(|s| s.parse().unwrap_or(0))
                        .collect();

                    let diffs: Vec<i32> = numbers.windows(2).map(|w| w[1] - w[0]).collect();
                    let min_diff = diffs.iter().map(|x| x.abs()).min().unwrap();
                    let max_diff = diffs.iter().map(|x| x.abs()).max().unwrap();
                    let check_diffs = (min_diff >= 1) && (max_diff <= 3);
                    let check_asc = diffs
                        .iter()
                        .map(|&x| x > 0)
                        .collect::<Vec<bool>>()
                        .iter()
                        .all(|&x| x);
                    let check_desc = diffs
                        .iter()
                        .map(|&x| x < 0)
                        .collect::<Vec<bool>>()
                        .iter()
                        .all(|&x| x);

                    let mut is_safe = false;
                    if check_diffs && (check_asc || check_desc) {
                        is_safe = true;
                    } else {
                        for i in 0..numbers.len() - 1 {
                            let numbers_2: Vec<i32> = numbers
                                .iter()
                                .enumerate()
                                .filter(|&x| x.0 != i)
                                .map(|x| *x.1)
                                .collect();
                            let diffs: Vec<i32> =
                                numbers_2.windows(2).map(|w| w[1] - w[0]).collect();
                            let min_diff = diffs.iter().map(|x| x.abs()).min().unwrap();
                            let max_diff = diffs.iter().map(|x| x.abs()).max().unwrap();
                            let check_diffs = (min_diff >= 1) && (max_diff <= 3);
                            let check_asc = diffs
                                .iter()
                                .map(|&x| x > 0)
                                .collect::<Vec<bool>>()
                                .iter()
                                .all(|&x| x);
                            let check_desc = diffs
                                .iter()
                                .map(|&x| x < 0)
                                .collect::<Vec<bool>>()
                                .iter()
                                .all(|&x| x);

                            if check_diffs && (check_asc || check_desc) {
                                is_safe = true;
                                break;
                            }
                        }
                    }
                    is_safe as usize
                })
                .sum()
        })
        .unwrap_or(0);
    result
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::utils::test_utils::init_logger;

    #[test]
    fn test1() {
        init_logger();
        assert_eq!(part1("assets/input-day02-00-test1.txt"), 2);
    }

    #[test]
    fn test2() {
        init_logger();
        assert_eq!(part2("assets/input-day02-00-test1.txt"), 4);
    }
}
