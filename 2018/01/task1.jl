function solve(input_file)
    frequencies = map(x-> parse(Int, x[2:length(x)]) * ((x[1] == '-') ? -1 : 1), readlines(input_file))

    total = 0
    for x in frequencies
        total += x
    end

    print("resulting frequency from '$input_file': $total")
end

solve("01test1.txt")
solve("01test2.txt")
solve("01test3.txt")
solve("01.txt")
