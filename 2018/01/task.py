import requests
import time
import numpy as np


r = requests.get('https://adventofcode.com/2018/day/1/input', cookies=cookie)

inlist_request = [x for x in r.text[:-1].split('\n') if x != '']

total = 0
for i in r.text.split('\n'):
    if i != '':
        if i[0] == '+':
            c
        elif i[0] == '-':
            n = -1*int(i[1:])
        total += n
print('part 1', total)
total = 0

print(sum([int(x) for x in r.text[:-1].split() if x != '']))

test1 = '+1\n-2\n+3\n+1\n' # first reaches 2 twice
test2 = '+1\n-1' #first reaches 0 twice.
test3 = '+3\n+3\n+4\n-2\n-4' #first reaches 10 twice.
test4 = '-6\n+3\n+8\n+5\n-6' #first reaches 5 twice.
test5 = '+7\n+7\n-2\n-7\n-4' #first reaches 14 twice.
inlist = [x for i in range(10) for x in test5.split('\n') if x != '']


total = 0
totals = [0]
i = 0
s = 0
inlist = [x for x in inlist_request if x != '']
while True:
    if inlist[i][0] == '+':
        n = int(inlist[i][1:])
    elif inlist[i][0] == '-':
        n = -int(inlist[i][1:])
    total += n
    #print(k, n, total)
    if total in totals:
        print('break at', total)
        break
    else:
        totals.append(total)
    i += 1
    if i == len(inlist):
        i = 0
        s += 1

print(sum([int(x) for x in r.text[:-1].split() if x != '']))


from itertools import accumulate, cycle
seen = set()
inlist = [int(x) for x in r.text[:-1].split() if x != '']
print(next(f for f in accumulate(cycle(inlist)) if f in seen or seen.add(f)))
