function solve(input_file)
    frequencies = map(x-> parse(Int, x[2:length(x)]) * ((x[1] == '-') ? -1 : 1), readlines(input_file))

    dict = Dict(0=>1)
    total = 0
    solved = false
    x = 1
    while true
        total += frequencies[x]
        if (total in keys(dict))
            dict[total] += 1
            solved = true
            break
        else
            dict[total] = 1
            if x == length(frequencies)
                x = 1
            else
                x += 1
            end
        end
    end
    print("first frequency to double '$input_file': ", solved ? "$total" : "no solution")
end

solve("02test1.txt")
solve("02test2.txt")
solve("02test3.txt")
solve("02test4.txt")
solve("01.txt")
