function solve(input_file)
    input = readlines(input_file)
    claims = Dict()
    sizes = Dict()
    xmax, ymax = 0, 0
    for el in input
        i, at_, offset, size = split(el, " ")
        idx = parse(Int, i[2:end])
        ox, oy = map(x->parse(Int, x), split(offset[1:end-1], ","))
        dx, dy = map(x->parse(Int, x), split(size, "x"))
        sizes[idx] = dx * dy
        for x in ox:(ox-1+dx)
            for y in oy:(oy-1+dy)
                if (x, y) in keys(claims)
                    claims[(x, y)] = 0
                else
                    claims[(x, y)] = idx
                end
                if x > xmax
                    xmax = x
                end
                if y > ymax
                    ymax = y
                end
            end
        end
    end
    counts = Dict()
    for v in values(claims)
        if v in keys(counts)
            counts[v] += 1
        else
            counts[v] = 1
        end
    end
    for (id, v) in sizes
        if (id in keys(counts))
            if (v == counts[id])
                println("Result of $input_file is $id")
                return
            end
        end
    end
end

solve("03test1.txt")
solve("03.txt")
