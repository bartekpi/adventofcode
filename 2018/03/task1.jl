function solve(input_file)
    input = readlines(input_file)
    claims = Dict()
    xmax, ymax = 0, 0
    for el in input
        i, at_, offset, size = split(el, " ")
        idx = parse(Int, i[2:end])
        ox, oy = map(x->parse(Int, x), split(offset[1:end-1], ","))
        dx, dy = map(x->parse(Int, x), split(size, "x"))
        for x in ox:(ox-1+dx)
            for y in oy:(oy-1+dy)
                if (x, y) in keys(claims)
                    claims[(x, y)] = "X"
                else
                    claims[(x, y)] = string(idx)
                end
                if x > xmax
                    xmax = x
                end
                if y > ymax
                    ymax = y
                end
            end
        end
    end
    # for x in 1:xmax
    #     s = ""
    #     for y in 1:ymax
    #         s *= (x, y) in keys(claims) ? claims[(x, y)] : "."
    #     end
    #     println(s)
    # end
    total = 0
    for k in keys(claims)
        if claims[k] == "X"
            total += 1   
        end
    end
    println("Result of $input_file is $total")
end

solve("03test1.txt")
solve("03.txt")
