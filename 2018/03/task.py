import requests
import time
import numpy as np


r = requests.get('https://adventofcode.com/2018/day/3/input', cookies=cookie)

input_day03 = [x for x in r.text.split('\n') if x != '']

input_day03_test = '''#1 @ 1,3: 4x4
#2 @ 3,1: 4x4
#3 @ 5,5: 2x2'''.split('\n')

print(input_day03_test)

inlist = input_day03 #_test

# 1st pass to determine grid size
areas = []
area_ids = {}
rows, cols = 0, 0
for el in inlist:
    #print(el)
    left, top = [int(x) for x in el.split()[2][:-1].split(',')]
    width, height = [int(x) for x in el.split()[3].split('x')]
    area_id = int(el.split()[0][1:])
    area_ids[area_id] = 0
    areas.append([area_id, left, top, width, height])
    if left + width > cols:
        cols = left + width
    if top + height > rows:
        rows = top + height


print(rows, cols)

dim = max(rows, cols)

grid = [[[] for r in range(dim)] for c in range(dim)]

for area_id, row, col, wi, hi in areas:
    for r in range(row, row+wi):
        for c in range(col, col+hi):
            grid[r][c].append(area_id)
            if len(grid[r][c]) > 1:
                for el in grid[r][c]:
                    area_ids[el] += 1

result = sum([1 if len(item) > 1 else 0 for sublist in grid for item in sublist])

print(result)

print([(x,y) for x, y in area_ids.items() if y == 0])
