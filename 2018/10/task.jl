function solve(input_file)
    raw_input = readlines(input_file)
    pos = []
    vel = []
    for (i, rawstring) in enumerate(raw_input)
        p, v = split(rawstring, "=")[2:3]
        p = p[1:(end-length(" velocity"))]

        px, py = map(x->parse(Int, x), split(p[2:(end-1)], ","))
        vx, vy = map(x->parse(Int, x), split(v[2:(end-1)], ","))

        if i == 1
            pos = [px py]
            vel = [vx vy]
        else
            pos = [pos ; [px py]]
            vel = [vel ; [vx vy]]
        end
    end
    
    dims = []
    step = 0
    xmax, ymax = maximum(pos, dims=1)
    xmin, ymin = minimum(pos, dims=1)
    xdim = xmax - xmin
    ydim = ymax - ymin
    push!(dims, xdim * ydim)
    while true
        step += 1
        pos += vel
        xmax, ymax = maximum(pos, dims=1)
        xmin, ymin = minimum(pos, dims=1)
        xdim = xmax - xmin
        ydim = ymax - ymin
        
        if (xdim * ydim <= dims[end])
            push!(dims, xdim * ydim)
        else
            pos -= vel
            xmax, ymax = maximum(pos, dims=1)
            xmin, ymin = minimum(pos, dims=1)
            rows = collect(eachrow(pos))
            println("Step $(step - 1) ($xdim, $ydim)")
            println(join([join([[x, y] in rows ? "#" : "." for x in xmin:xmax], "") for y in ymin:ymax], "\n"))
            break
        end

        if (step > 100000)
            break
        end
    end
end

solve("10.txt")
