function solve(input_file)
    input = readlines(input_file)[1]
    # println("string: ", input[1:10])
    i = 1
    while (true)
        if i > length(input) - 1
            break
        end
        s1 = input[i]
        s2 = input[i+1]
        if (s1 != s2) & (lowercase(s1) == lowercase(s2))
            input = input[1:i-1] * input[i+2:end]
            # println("(", string(i), ") string now: ", input)
            i = max(1, i - 1)
        else
            i += 1
        end
    end
    println(input_file, " resulting polymer contains ", string(length(input)), " units")
end

solve("05test1.txt")
solve("05test2.txt")
solve("05.txt")
