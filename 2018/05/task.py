import requests
import time
import numpy as np


with open('inputs/2018_05.01.txt', 'r') as f:
    input_day = f.read()

input_test = 'dabAcCaCBAcCcaDA'

[Counter(x) for x in data_in]

def remove_polar_opposites(string):
    output = [x for x in string]
    for i, char in enumerate(output):
        if i > 0:
            prev_char = output[i-1]
            if char.lower() == prev_char.lower() and char != prev_char:
                output[i] = ''
                output[i-1] = ''
    return ''.join(output)

assert remove_polar_opposites('aA') == ''
assert remove_polar_opposites('abBA') == 'aA'
assert remove_polar_opposites('abAB') == 'abAB'
assert remove_polar_opposites('aabAAB') == 'aabAAB'

def solve_05(string):
    output = string
    if remove_polar_opposites(output) == output:
        return len(output)
    else:
        output = remove_polar_opposites(output)
        return solve_05(output)

assert solve_05(input_test) == 10

print(solve_05(input_day))
