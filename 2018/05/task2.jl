function solve(input_file)
    raw_input = readlines(input_file)[1]
    results = zeros(Int, 0)
    for c in Set(lowercase(raw_input))
        cu = uppercase(c)
        input = replace(raw_input, c => "")
        input = replace(input, cu => "")
        # println("string: ", input)
        i = 1
        while (true)
            if i > length(input) - 1
                break
            end
            s1 = input[i]
            s2 = input[i+1]
            if (s1 != s2) & (lowercase(s1) == lowercase(s2))
                input = input[1:i-1] * input[i+2:end]
                i = max(1, i - 1)
            else
                i += 1
            end
        end
        # println(input_file, " resulting polymer contains ", string(length(input)), " units")
        push!(results, length(input))
    end
    println(input_file, " shortest resulting polymer contains ", minimum(results), " units")
end

solve("05test1.txt")
solve("05.txt")
