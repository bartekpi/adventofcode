with open("08.txt", "r") as f:
    raw_input = f.read().strip()
data = [int(x) for x in raw_input.split(" ")]

verbose = False


def parse(data, it):
    it += 1
    if verbose:
        print("\t"*it, f"[{it}] running on:", data)
    children, metas = data[:2]
    data = data[2:]
    totals = 0
    scores = []
    if verbose:
        print("\t"*it, f"[{it}] children:", children)
        print("\t"*it, f"[{it}] metas:", metas)
        print("\t"*it, f"[{it}] data:", data)
    for i in range(children):
        total, score, data = parse(data, it)
        totals += total
        scores.append(score)

    if verbose:
        print("\t"*it, f"[{it}] summing up", data[:metas])
        print("\t"*it, f"[{it}] scores", scores)
        print()
    totals += sum(data[:metas])

    if children == 0:
        result = (totals, sum(data[:metas]), data[metas:])
    else:
        if verbose:
            print("\t"*it, f"[{it}] result data:", data)
            print("\t"*it, f"[{it}] result metadata:", metas)
            print("\t"*it, f"[{it}] result scores:", scores)

        result = (
            totals,
            sum(scores[k - 1] for k in data[:metas] if k > 0 and k <= len(scores)),
            data[metas:]
        )

    return result


total, value, remaining = parse(data, 0)
print('total:', total)
print('remaining:', remaining)
print('value:', value)
