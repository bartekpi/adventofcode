with open("08.txt", "r") as f:
    raw_input = f.read().strip()
data = [int(x) for x in raw_input.split(" ")]

verbose = False


def parse(data, it):
    it += 1
    if verbose:
        print("\t"*it, f"[{it}] running on:", data)
    children, metas = data[:2]
    data = data[2:]
    totals = 0
    if verbose:
        print("\t"*it, f"[{it}] children:", children)
        print("\t"*it, f"[{it}] metas:", metas)
        print("\t"*it, f"[{it}] data:", data)
    for i in range(children):
        total, data = parse(data, it)
        totals += total

    if verbose:
        print("\t"*it, f"[{it}] summing up", data[:metas])
        print()
    totals += sum(data[:metas])

    return (totals, data[metas:])


total, remaining = parse(data, 0)
print('total:', total)
print('remaining:', remaining)
