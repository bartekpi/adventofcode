function solve(input_file; verbose=false)
    raw_input = readlines(input_file)[1]
    data = map(x->parse(Int, x), split(raw_input, " "))

    function parse_data(data, it)
        it += 1
        if verbose
            println("\t"^it, "[$it] running on: $data")
        end
        children, metadata = data[1:2]
        data = data[3:end]
        total_sum = 0
        scores = []
        if verbose
            println(repeat("\t", it), "[$it] children: $children")
            println(repeat("\t", it), "[$it] metadata: $metadata")
            println(repeat("\t", it), "[$it] data: $data")
        end
        if children > 0
            for i in 1:children
                total, score, data = parse_data(data, it)
                total_sum += total
                push!(scores, score)
            end
        end
        if verbose
            println(repeat("\t", it), "[$it] summing up: ", data[1:metadata])
            println(repeat("\t", it), "[$it] scores: ", scores)
            println()
        end
        total_sum += sum(data[1:metadata])

        if (children == 0)
            result = (
                total_sum,
                sum(data[1:metadata]),
                data[(metadata+1):end]
            )
        else
            if verbose
                println(repeat("\t", it), "[$it] result data: $data")
                println(repeat("\t", it), "[$it] result metadata: $metadata")
                println(repeat("\t", it), "[$it] result scores: $scores")
            end
            result = (
                total_sum,
                # sum(scores[k] for k in data[1:metadata] if (k > 0) & (k <= length(scores))),
                sum([((k > 0) & (k <= length(scores))) ? scores[k] : 0 for k in data[1:metadata]]),
                data[(metadata+1):end]
            )
        end

        return result
    end

    total, value, data = parse_data(data, 0)
    println("result: $total")
    println("value: $value")
end

solve("08test1.txt", verbose=true)
solve("08.txt")
