function solve(input_file; verbose=false)
    raw_input = readlines(input_file)[1]
    data = map(x->parse(Int, x), split(raw_input, " "))

    function parse_data(data, it)
        it += 1
        if verbose
            println("\t"^it, "[$it] running on: $data")
        end
        children, metadata = data[1:2]
        data = data[3:end]
        total_sum = 0
        if verbose
            println(repeat("\t", it), "[$it] children: $children")
            println(repeat("\t", it), "[$it] metadata: $metadata")
            println(repeat("\t", it), "[$it] data: $data")
        end
        if children > 0
            for i in 1:children
                total, data = parse_data(data, it)
                total_sum += total
            end
        end
        if verbose
            println(repeat("\t", it), "[$it] summing up: ", data[1:metadata], "\n")
        end
        total_sum += sum(data[1:metadata])

        return (total_sum, data[(metadata+1):end])
    end

    total, data = parse_data(data, 0)
    println("result: $total")
end

solve("08test1.txt", verbose=true)
solve("08.txt")
