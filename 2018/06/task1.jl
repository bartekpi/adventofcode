function solve(input_file)
    raw_input = readlines(input_file)
    points = [map(x->parse(Int, x), split(x, ", ")) for x in raw_input]
    points_matrix = reshape(collect(Iterators.flatten(points)), (2, length(points)))'

    # alternative way:   
    # points = []
    # for pp in raw_input
    #     x, y = split(pp, ", ")
    #     push!(points, [parse(Int, x), parse(Int, y)])
    # end
    
    min_x, min_y = minimum(points_matrix, dims=1)
    max_x, max_y = maximum(points_matrix, dims=1)
    
    rows = max_x
    cols = max_y

    grid = [[0 for y in 1:cols] for x in 1:rows]

    for (i, (x, y)) in enumerate(points)
        grid[x][y] = i
    end

    # print(join([join(x, "") for x in grid], "\n"))
    counts = Dict((i=>1 for i in 1:length(points)))

    for row in min_x:rows
        for col in min_y:cols
            if grid[row][col] == 0
                dists = Dict()
                for (i, (px, py)) in enumerate(points)
                    dists[i] = abs(px - row) + abs(py - col) 
                end
                mins = [(k, v) for (k, v) in dists if v == minimum(values(dists))]
                if length(mins) > 1
                    grid[row][col] = 0
                else
                    grid[row][col] = mins[1][1]
                    counts[mins[1][1]] += 1
                end
            end
        end
    end

    off_limits = union!(
        Set(grid[min_x]),
        Set(grid[max_x]),
        Set([x[min_y] for x in grid]),
        Set([x[max_y] for x in grid])
    )
    candidates = [(k, v) for (k, v) in counts if !(k in off_limits)]
    max_area = maximum([x[2] for x in candidates])
    println("Largest area size: $max_area")
end

solve("05test1.txt")
solve("05.txt")
