function solve(input_file, max_size)
    raw_input = readlines(input_file)
    points = [map(x->parse(Int, x), split(x, ", ")) for x in raw_input]
    points_matrix = reshape(collect(Iterators.flatten(points)), (2, length(points)))'

    # alternative way:   
    # points = []
    # for pp in raw_input
    #     x, y = split(pp, ", ")
    #     push!(points, [parse(Int, x), parse(Int, y)])
    # end
    
    min_x, min_y = minimum(points_matrix, dims=1)
    max_x, max_y = maximum(points_matrix, dims=1)
    
    rows = max_x
    cols = max_y

    grid = [[0 for y in 1:cols] for x in 1:rows]

    for (i, (x, y)) in enumerate(points)
        grid[x][y] = i
    end

    for row in min_x:rows
        for col in min_y:cols
            dists = 0
            for (i, (px, py)) in enumerate(points)
                dists += abs(px - row) + abs(py - col) 
            end
            if dists < max_size
                grid[row][col] = 1
            else
                grid[row][col] = 0
            end
        end
    end
    area_size = sum(sum(grid))
    println("Area size: $area_size")
end

solve("05test1.txt", 32)
solve("05.txt", 10000)
