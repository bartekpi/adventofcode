import requests
import time
import numpy as np

r= requests.get('https://adventofcode.com/2018/day/4/input', cookies=cookie)

input_day03_part1 = [x for x in r.text.split('\n') if x != '']

input_day03_test1 = '''[1518-11-01 00:00] Guard #10 begins shift
[1518-11-01 00:05] falls asleep
[1518-11-01 00:25] wakes up
[1518-11-01 00:30] falls asleep
[1518-11-01 00:55] wakes up
[1518-11-01 23:58] Guard #99 begins shift
[1518-11-02 00:40] falls asleep
[1518-11-02 00:50] wakes up
[1518-11-03 00:05] Guard #10 begins shift
[1518-11-03 00:24] falls asleep
[1518-11-03 00:29] wakes up
[1518-11-04 00:02] Guard #99 begins shift
[1518-11-04 00:36] falls asleep
[1518-11-04 00:46] wakes up
[1518-11-05 00:03] Guard #99 begins shift
[1518-11-05 00:45] falls asleep
[1518-11-05 00:55] wakes up'''.split('\n')

with open("04.txt", "r") as f:
    input_day03 = f.read().split("\n")[:-1]
input_day03 = input_day03_test1
timeline_guards = sorted([x for x in input_day03 if x.split(' ')[2] == 'Guard'])
timeline_sleeps = sorted([x for x in input_day03 if x.split(' ')[2] != 'Guard'])
sleeps = np.array([[0 for x in range(60)] for y in range(len(timeline_guards))])

days = []
#print('''Date   ID   Minute
#            000000000011111111112222222222333333333344444444445555555555
#            012345678901234567890123456789012345678901234567890123456789
#''')
guards = {}
for i, el in enumerate(timeline_guards):
    year = el.split(' ')[0].split('-')[0][1:]
    month = el.split(' ')[0].split('-')[1]
    day = el.split(' ')[0].split('-')[2]
    hour = el.split(' ')[1].split(':')[0]
    if hour != '00':
        day = '{:02d}'.format(int(day) + 1)
    guard = el.split(' ')[3]
    if guard not in guards.keys():
        guards[guard] = [i]
    else:
        guards[guard].append(i)
    dt = '{}-{}-{}'.format(year, month, day)
    s = [x for x in timeline_sleeps if x[1:11]==dt]
    fa, wu = [], []
    for el in s:
        if el.split(' ')[2] == 'falls':
            # print('falls append', int(el.split(' ')[1].split(':')[1][:-1]))
            fa.append(int(el.split(' ')[1].split(':')[1][:-1]))
        if el.split(' ')[2] == 'wakes':
            # print('wakes append', int(el.split(' ')[1].split(':')[1][:-1]))
            wu.append(int(el.split(' ')[1].split(':')[1][:-1])) 

    ts = np.zeros(60)
    for a, b in zip(fa, wu):
        ts[a:b] = 1
    sleeps[i] = ts
    s_ = ''.join(['{:0.0f}'.format(x) for x in ts])
    
    k = '{}-{}  {}  {}'.format(month, day, guard, s_)
    #print(k)
    days.append(k)

max_sleep, max_guard, max_h = 0, '', 0
for guard in guards.keys():
    guard_sleep_total = sleeps[guards[guard],:].sum(axis=0).sum()
    if guard_sleep_total > max_sleep:
        max_sleep = guard_sleep_total
        max_guard = guard
        max_h = sleeps[guards[guard],:].sum(axis=0).argmax()
    #print(guard, guard_sleep_total)
print(max_guard, max_h, int(float(max_guard[1:])*max_h))

max_m, max_guard = 0, ''
for guard in guards.keys():
    guard_sleep_m = sleeps[guards[guard], :].sum(axis=0).argmax()
    if guard_sleep_m > max_m:
        max_m = guard_sleep_m
        max_guard = guard
print(max_guard, max_m, int(float(max_guard[1:])*max_m))
