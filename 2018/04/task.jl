function solve(input_file)
    input = sort(readlines(input_file))

    timeline_guards = filter(x->split(x, " ")[3] == "Guard", input)
    timeline_sleeps = filter(x->split(x, " ")[3] != "Guard", input)    
    sleeps = [zeros(Int64, 60) for x in 1:length(timeline_guards)]

    days = Vector{String}()
    guards = Dict()
    
    for (i, line) in enumerate(timeline_guards)
        items = split(line, " ")
        date = items[1][2:end]
        year, month, day = split(date, "-")
        hour = split(items[2], ":")[1]
        if hour != "00"
            day = @sprintf("%.02d", parse(Int, day) + 1)
        end
        guard = items[4]
    
        if (guard in keys(guards))
            push!(guards[guard], i)
        else
            guards[guard] = [i]
        end
        dt = "$year-$month-$day"

        sched = Dict("falls"=>[], "wakes"=>[])
        for el in filter(x->x[2:11]==dt, timeline_sleeps)
            act = split(el, " ")[3]
            t = parse(Int, split(split(el, " ")[2][1:end-1], ":")[2])
            push!(sched[act], t)
        end
        
        ts = zeros(Int64, 60)
        for (a, b) in zip(sched["falls"], sched["wakes"])
            for k in a+1:b
                ts[k] = 1
            end
        end
        sleeps[i] = ts
        tsstring = join(ts)
        push!(days, "$month-$day $guard $tsstring")
    end

    max_sleep , max_guard, max_h = 0, "", 0
    for guard in keys(guards)
        guard_sleep = sum(getindex(sleeps, guards[guard]), dims=1)[1]
        guard_sleep_total = sum(guard_sleep)
        if guard_sleep_total > max_sleep
            max_sleep = guard_sleep_total
            max_guard = guard
            max_h = argmax(guard_sleep) - 1
        end
    end
    result = parse(Int, max_guard[2:end]) * max_h
    message = "Result for $input_file [1]: $max_guard * $max_h = $result"
    println(message)

    max_m, max_guard = 0, ""
    for guard in keys(guards)
        guard_sleep = sum(getindex(sleeps, guards[guard]), dims=1)[1]
        guard_sleep_m = argmax(guard_sleep) - 1
        if guard_sleep_m > max_m
            max_m = guard_sleep_m
            max_guard = guard
        end
    end
    result = parse(Int, max_guard[2:end]) * max_m
    message = "Result for $input_file [2]: $max_guard * $max_m = $result"
    println(message)
end

solve("04test1.txt")
solve("04.txt")
