function solve(input_file)
    box_ids = readlines(input_file)
    
    dict = Dict(2=>0, 3=>0)
    for box_id in box_ids
        cdict = Dict()
        for c in box_id
            if (c in keys(cdict))
                cdict[c] += 1
            else
                cdict[c] = 1
            end
        end
        for v in intersect(Set(values(cdict)), Set([2, 3]))
            dict[v] += 1
        end
    end
    result_msg = string("$input_file checksum is ", dict[2], " * ", dict[3], " = ", dict[2] * dict[3])
    println(result_msg)
end

solve("02test1.txt")
solve("02.txt")
