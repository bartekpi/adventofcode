import requests
import time
import numpy as np


data_in = r.text.split()

#test
data_in = ['abcdef', 'bababc', 'abbcde', 'abcccd', 'aabcdd', 'abcdee', 'ababab']

from collections import Counter

result = [0, 0]
for row in data_in:
    counts = Counter(row)
    if 2 in counts.values():
        result[0] += 1
    if 3 in counts.values():
        result[1] += 1
        
print(result, result[0]*result[1])

#test
data_in = '''
abcde
fghij
klmno
pqrst
fguij
axcye
wvxyz
'''.split()

from itertools import combinations



result = []
max_sc = 0
for p in combinations(data_in, 2):
    sc = 0
    dif = []
    for i, (ch1, ch2) in enumerate(zip(p[0], p[1])):
        if ch1 == ch2:
            sc += 1
        else:
            dif.append(i)
    result.append((p, sc, dif))
    if sc > max_sc:
        max_sc = sc

print(max_sc)

for el in result:
    if el[1]==max_sc:
        print(el)
        print(el[0][0][:c]+el[0][0][c+1:])
