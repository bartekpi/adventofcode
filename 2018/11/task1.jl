function solve(input)
    input = 3628
    println("solving $(input)")
    rows = cols = 14
    grid = [[0 for y in 1:cols] for x in 1:rows]
    # get (X, Y) as grid[X][Y]
    grid[1][1] = 2
    grid[1][14] = 10
    grid
end

# solve(3628)
