# function solve(input_string; verbosity=0)
#     input_list = split(input_string, " ")
#     players = parse(Int, input_list[1])
#     last_marble = parse(Int, input_list[7])
#     
#     scores = Dict(x=>[] for x in 1:players)
#     marbles = [0]
#     updated_spot = 1
#     next_spot = 2
#     if verbosity > 0
#         println("[0] (0)")
#         if verbosity > 1
#             println("\tnext spot: $next_spot, list length: $(length(marbles))")
#         end
#     end
#     for marble in 1:last_marble
#         player = marble % players
#         if player == 0
#             player = 9
#         end
#         if (marble % 23 != 0)
#             if next_spot == (length(marbles) + 1)
#                 if verbosity > 1
#                     println("\t\t add $marble -> to the end of the list")
#                 end
#                 push!(marbles, marble)
#                 updated_spot = length(marbles)
#             elseif next_spot > (length(marbles) + 1)
#                 if verbosity > 1
#                     println("\t\t add $marble -> to the beginning")
#                 end
#                 marbles = [[marbles[1]]; [marble]; marbles[2:end]] 
#                 updated_spot = 2
#             else
#                 if verbosity > 1
#                     println("\t\t add $marble -> 2 steps right")
#                 end
#                 marbles = [marbles[1:(next_spot - 1)]; [marble]; marbles[next_spot:end]]
#                 updated_spot = next_spot
#             end
#         else
#             updated_spot = next_spot
#         end
#         next_spot = updated_spot + 2
# 
#         if verbosity > 0
#             marbles_str = join([(i == updated_spot) ? "($x)" : " $x " for (i, x) in enumerate(marbles)], " ")
#             println("($marble)[$player] ", marbles_str)
#         end
# 
#         if ((next_spot <= length(marbles)) & ((marble + 1) % 23 == 0))
#             next_spot = updated_spot - 7
#             if next_spot < 1
#                 next_spot = length(marbles) - 3
#             end
#             # println("\t\t\t next spot to update: $next_spot")
#             push!(scores[player], 23)
#             push!(scores[player], marbles[next_spot])
#             deleteat!(marbles, [next_spot])
#         end
#     end
# 
#     total_score = 0
#     for score in values(scores)
#         if length(score) > 0
#             total_score += sum(score)
#         end
#     end
#     if verbosity > 0
#         if verbosity > 1
#             println("scores: $scores")
#         end
#         println("score: $total_score")
#     end
#     return total_score
# end
# 
# @assert solve("9 players; last marble is worth 25 points", verbosity=1) == 32
# 
# @assert solve("10 players; last marble is worth 1618 points") == 8317
# @assert solve("13 players; last marble is worth 7999 points") == 146373
# @assert solve("17 players; last marble is worth 1104 points") == 2764
# @assert solve("21 players; last marble is worth 6111 points") == 54718
# @assert solve("9 players; last marble is worth 5807 points") == 37305

# solve("464 players; last marble is worth 71730 points")
