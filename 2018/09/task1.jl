verbosity = 1

input_string = "9 players; last marble is worth 25 points"

input_list = split(input_string, " ")
players = parse(Int, input_list[1])
last_marble = parse(Int, input_list[7])

scores = Dict(x=>0 for x in 1:players)
marbles = [0]

if verbosity > 0
    println("[-] (0)")
    if verbosity > 1
        println("\tnext spot: $next_spot, list length: $(length(marbles))")
    end
end

println("[-] ", UNDERLINE("0"))
# highlighted_marble = 0
# for current_marble in 1:last_marble
#     player = current_marble % players
#     player = player == 0 ? 9 : player
# 
#     # push!(marbles, current_marble)
#     # highlighted_marble = 1 + ((highlighted_marble + 2) % length(marbles))
#     # if verbosity > 0
#     #     marbles_str = join([x == highlighted_marble ? UNDERLINE(string(y)) : string(y) for (x, y) in enumerate(marbles)], " # ")
#     #     println("[$player] $marbles_str")
#     # end
#     c = readline()
# end
