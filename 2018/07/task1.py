def solve_07(file_in, verbose=False):
    with open(file_in, 'r') as f:
        input_string = f.read()
    inputs = [
        (x.split(' ')[1], x.split(' ')[-3])
        for x in input_string.split('\n') if len(x) > 0
    ]

    # set of all available letters
    steps = set([x for x, y in inputs] + [y for x, y in inputs])
    print("all the steps:", steps)
    order = ""
    while steps:
        # get all letters that don't depend on other letters
        candidates = [
            x for x in steps if all(y != x for (_, y) in inputs)
        ]
        # these are the candidates that we will sort and chose alphabetically
        print("candidates:", candidates)
        candidates.sort()
        candidate = candidates[0]
        order += candidate
        steps.remove(candidate)
        # once we chose it we neeed to remove it from possibilities
        # and run the thing again until we've assigned all letters
        inputs = [(x, y) for (x, y) in inputs if x != candidate]
        print("remaining inputs:", inputs)
        print("remaining steps:", steps)

    print("result is:", order)


solve_07("07test1.txt", True)
solve_07("07.txt", True)
