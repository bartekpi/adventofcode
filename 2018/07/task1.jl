function solve(input_file, verbose=false)
    raw_input = readlines(input_file)
    
    inputs = [
        (split(line, " ")[2], split(line, " ")[8])
        for line in raw_input
    ]

    steps = union(
        Set([x[1] for x in inputs]),
        Set([x[2] for x in inputs])
       )
    # println("all the steps: ", steps)
    order = ""
    while length(steps) > 0
        # get letters that don't depend on others
        candidates = sort([x for x in steps if all(y[2] != x for y in inputs)])
        # println("candidates: ", candidates)
        candidate = candidates[1]

        order *= candidate
        delete!(steps, candidate)

        # update inputs so the ones ordered are no longer in the pool
        inputs = [(x, y) for (x, y) in inputs if x != candidate]
        # println("remaining inputs: ", inputs)
        # println("remaining steps: ", steps)
    end
    
    println("result is: ", order)
end

solve("07test1.txt")
solve("07.txt")
