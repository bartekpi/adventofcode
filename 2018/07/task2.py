def solve_07(file_in, time_offset=0, n_workers=1, verbose=False):
    with open(file_in, 'r') as f:
        input_string = f.read()
    inputs = [
        (x.split(' ')[1], x.split(' ')[-3])
        for x in input_string.split('\n') if len(x) > 0
    ]

    # set of all available letters
    steps = set([x for x, y in inputs] + [y for x, y in inputs])
    time = {x: time_offset+i for (i, x) in enumerate(sorted(steps))}
    order = ""
    t = 0
    workers = [0 for _ in range(n_workers)]
    work = [None for _ in range(n_workers)]
    if verbose:
        print(
            "Second\t",
            "\t".join([f"Worker {i}" for i in range(n_workers)]),
            "\tDone"
        )
    while steps or any(w > 0 for w in workers):
        # get all letters that don't depend on other letters
        candidates = [
            x for x in steps if all(y != x for (_, y) in inputs)
        ]
        candidates.sort()
        candidates = candidates[::-1]

        for i in range(n_workers):
            workers[i] = max(workers[i] - 1, 0)
            if workers[i] == 0:
                if work[i] is not None:
                    inputs = [(x, y) for (x, y) in inputs if x != work[i]]
                if candidates:
                    n = candidates.pop()
                    workers[i] = time[n]
                    work[i] = n
                    steps.remove(n)
                    order += n
        t += 1
        if verbose:
            print("{}\t{}\t\t{}".format(
                t,
                "\t\t".join([work[i] if work[i] is not None else "" for i in range(n_workers)]),
                order
            ))

    print("result is:", t)


solve_07("07test1.txt", n_workers=2, time_offset=0, verbose=True)
solve_07("07.txt", n_workers=5, time_offset=60, verbose=False)
