function solve(input_file; n_workers=1, time_offset=0, verbose=false)
    raw_input = readlines(input_file)

    inputs = [
        (split(line, " ")[2], split(line, " ")[8])
        for line in raw_input
    ]

    steps = union(
        Set([x[1] for x in inputs]),
        Set([x[2] for x in inputs])
       )
    time = Dict(x=>time_offset+i-1 for (i, x) in enumerate(sort([x for x in steps])))

    order = ""
    t = 0
    workers = zeros(Int, n_workers)
    work = ["" for x in 1:n_workers]
    if verbose
        println("Second\t", join(["Worker $i" for i in 1:n_workers], "\t"), "\tDone")
    end
    while (length(steps) > 0) | any(w > 0 for w in workers)
        # get letters that don't depend on others
        candidates = [x for x in steps if all(y[2] != x for y in inputs)]
        candidates = sort(candidates, rev=true)

        for i in 1:n_workers
            # reduce the time each worker has to spend on current task
            workers[i] = max(workers[i] - 1, 0)

            if workers[i] == 0
                if work[i] != ""
                    inputs = [(x, y) for (x, y) in inputs if x != work[i]]
                end
                if length(candidates) > 0
                    n = pop!(candidates)
                    workers[i] = time[n]
                    work[i] = n
                    delete!(steps, n)
                    order *= n
                end
            end
        end

        if verbose
            println("$t\t", join([work[i] for i in 1:n_workers], "\t\t"), "\t\t$order")
        end
        t += 1
    end
    
    println("result is: ", t)
end

solve("07test1.txt", n_workers=2, time_offset=0, verbose=true)
solve("07.txt", n_workers=5, time_offset=60)
