function solve(input_file)
    lines = Array([[parse(Int, y) for y in x] for x in readlines(input_file)])
    r = length(lines)
    c = maximum([length(x) for x in lines])
    # oxygen generator rating
    bits = zeros(Int, r, c)
    for (i, line) in enumerate(lines)
        bits[i, :] = line
    end
    for i in 1:c
        # println("$i $(size(bits))")
        # display(bits)
        s = sum(bits[:, i])
        p = 1*(s >= (size(bits)[1]/2))
        # println("most common is $p")
        bits = bits[bits[:, i] .== p, :]
        if size(bits)[1] == 1
            break
        end
    end
    rating_o = parse(Int, join(bits, ""), base=2)
    # co2 scrubber rating
    bits = zeros(Int, r, c)
    for (i, line) in enumerate(lines)
        bits[i, :] = line
    end
    for i in 1:c
        # println("$i $(size(bits))")
        # display(bits)
        s = sum(bits[:, i])
        p = 1 - 1*(s >= (size(bits)[1]/2))
        # println("most common is $p")
        bits = bits[bits[:, i] .== p, :]
        if size(bits)[1] == 1
            break
        end
    end
    rating_co2 = parse(Int, join(bits, ""), base=2)

    println("$rating_o x $rating_co2 = $(rating_o*rating_co2)")
end

solve("01test.txt")
solve("01.txt")
