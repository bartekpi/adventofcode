function solve(input_file)
    lines = Array([[parse(Int, y) for y in x] for x in readlines(input_file)])
    r = length(lines)
    c = maximum([length(x) for x in lines])
    bits = zeros(Int, r, c)
    for (i, line) in enumerate(lines)
        bits[i, :] = line
    end
    gammas, epsilons = [], []
    m = r / 2
    for i in 1:c
        s = sum(bits[:, i])
        p = 1*(s > m)
        push!(gammas, p)
        push!(epsilons, 1-p)
    end
    gamma = parse(Int, join(gammas, ""), base=2)
    epsilon = parse(Int, join(epsilons, ""), base=2)
    println("$gamma x $epsilon = $(gamma*epsilon)")
end

solve("01test.txt")
solve("01.txt")
