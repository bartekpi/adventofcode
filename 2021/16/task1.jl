function solve(input_file)
    raw_data = readline(input_file)
    hex2bin = Dict(uppercase(string(x, base=16)) => string(x, base=2, pad=4) for x in 0:15)
    decoded_data = join([hex2bin["$x"] for x in raw_data], "")
    letters = map(x -> x |> string |> uppercase, collect('a':'z'))
    # for i in 1:(length(decoded_data)-2)
        # println(decoded_data[i:i+2])
    # end
    packet_version = parse(Int, "0b" * decoded_data[1:3])
    packet_type_id = parse(Int, "0b" * decoded_data[4:6])
    s = 7
    d = 5
    i = 1
    labelled_data = "VVVTTT"
    literal_values = []
    while true
        push!(literal_values, decoded_data[(s + 1):(s + d - 1)])
        labelled_data *= letters[i] ^ d
        if decoded_data[s] == '1'
            s += d
            i += 1
        else
            break
        end
    end
    println(decoded_data, "\n", labelled_data)
end

# solve("01test1.txt")
