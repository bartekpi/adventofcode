function solve(input_file)
    instructions = []
    for line in readlines(input_file)
        c1, c2 = split(line, "->")
        x1, y1 = map(x->parse(Int, x), split(c1, ","))
        x2, y2 = map(x->parse(Int, x), split(c2, ","))
        push!(instructions, [(x1, y1), (x2, y2)])
    end
    x_max = maximum([maximum([x[1], y[1]]) for (x, y) in instructions])
    y_max = maximum([maximum([x[2], y[2]]) for (x, y) in instructions])
    dim = maximum([x_max, y_max])
    space = zeros(Int, dim+1, dim+1)

    for (i, ((x1, y1), (x2, y2))) in enumerate(instructions)
        if ((x1 != x2) & (y1 != y2))
            continue
        end
        # println("Step $i: ($x1, $y1) -> ($x2, $y2)")
        dx = x2 - x1
        dy = y2 - y1
        dxi = (-1)^(dx < 0)
        dyi = (-1)^(dy < 0)
        for x = 0:dxi:dx, y = 0:dyi:dy
            # println("Add 1 to ($(y1+1+y), $(x1+1+x))")
            space[y1+1+y, x1+1+x] += 1
        end
        # display(space)
    end
    # display(space)
    println("Result is $(sum(space .> 1))")
end

solve("01test.txt")
solve("01.txt")
