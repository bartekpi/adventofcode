function solve(input_file; max_i=10)
    cavern = hcat(map(x->map(x->parse(Int, x), collect(x)), readlines(input_file))...)'
    rows = size(cavern)[1]
    cols = size(cavern)[2]
    # println("Before any steps:")
    # display(cavern)
    i = 0
    total_flashed = 0
    while true
        i += 1
        cavern = cavern .+ 1
        flashed = [x for x in (findall(x->x>9, cavern))]
        # println("after incrementing:")
        # display(cavern)
        for idx in flashed
            # println("checking for $(idx)")
            row_span = maximum([1, idx[1]-1]):minimum([rows, idx[1]+1])
            col_span = maximum([1, idx[2]-1]):minimum([cols, idx[2]+1])
            for r=row_span, c=col_span            
                if ~(CartesianIndex(r, c) in flashed)
                    cavern[r, c] += 1
                    # println("zero at ($(idx[1]), $(idx[2])) incremented ($r, $c) to $(cavern[r, c])")
                    if cavern[r, c] > 9
                        push!(flashed, CartesianIndex(r, c))
                        # println("added ($r, $c) to list of flashed. now it's $([(idx[1], idx[2]) for idx in flashed])")
                    end
                end
            end
        end
        cavern[cavern .== 10] .= 0
        # println("After step $i:")
        # display(cavern)
        # s = readline()
        # if s == "x"
            # break
        # end
        total_flashed += length(flashed)
        if i == max_i
            break
        end
     end
    println("Total flashes: $total_flashed")
end

solve("01test.txt", max_i=10)
solve("01test2.txt", max_i=10)
solve("01test2.txt", max_i=100)

solve("01.txt", max_i=100)
