function solve(input_file)
    fish = map(x->parse(Int, x), split(readline(input_file), ","))
    # println("Initial state $(join(fish, ","))")
    fishalt = map(x->parse(Int, x), split(readline(input_file), ","))
    max_d = 10
    for d in 1:max_d
        println("$d/$max_d")
        if sum(fish .== 0) > 0
            new_fish = ones(Int, sum(fish .== 0))*8
            # println("... add $(new_fish)")
        else
            new_fish = []
        end
        for (i, f) in enumerate(fish)
            if f == 0
                fish[i] = 6
            else
                fish[i] = f - 1
            end
        end
        fishalt = (fishalt .- 1) .% 7 .+ (fishalt .== 0)*7
        if length(new_fish) > 0
            fish = [fish; new_fish]
            fishalt = [fishalt; new_fish]
        end
        if d <= 18
            # println("After $d days: $(join(fish, ","))")
            println("fish 1: $(join(fish, ","))")
            println("fish 2: $(join(fishalt, ","))")
        end
    # new_fish = ones(Int, sum(fish .== 0))*8
    end
    println("There are $(length(fish)) lanternfish after $max_d days")
end

solve("01test.txt")
solve("01.txt")
