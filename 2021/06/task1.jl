function solve(input_file)
    fish = map(x->parse(Int, x), split(readline(input_file), ","))
    # println("Initial state $(join(fish, ","))")
    max_d = 80
    for d in 1:max_d
        if sum(fish .== 0) > 0
            new_fish = ones(Int, sum(fish .== 0))*8
            # println("... add $(new_fish)")
        else
            new_fish = []
        end
        for (i, f) in enumerate(fish)
            if f == 0
                fish[i] = 6
            else
                fish[i] = f - 1
            end
        end
        if length(new_fish) > 0
            fish = [fish; new_fish]
        end
        if d <= 18
            nothing
            # println("After $d days: $(join(fish, ","))")
        end
    # new_fish = ones(Int, sum(fish .== 0))*8
    end
    println("There are $(length(fish)) lanternfish after $max_d days")
end

solve("01test.txt")
solve("01.txt")
