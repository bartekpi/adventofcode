"""shamelessly stolen from stackoverflow"""
function permute(array::T, prefix=T()) where T
    if length(array) == 1
        return [[prefix; array]]
    else
        t = T[]
        for i in eachindex(array)
            if i > firstindex(array) && array[i] == array[i-1]
                continue
            end
            append!(t, unique_permutations([array[begin:i-1];array[i+1:end]], [prefix; array[i]]))
        end
        return t
    end
end

function solve(input_file)
    cavern = hcat(map(x->map(x->parse(Int, x), collect(x)), readlines(input_file))...)'
    rows = size(cavern)[1]
    cols = size(cavern)[2]
    # steps = split("D"^(rows-1) * "R"^(cols-1), "")
    # lowest_risk = sum(cavern)
    # for path in permute(steps)
    #     # println("Checking path: $(join(path, ""))")
    #     position = [1, 1]
    #     s = 0  # cavern[position...]
    #     for p in path
    #         # println("\tgoing $p")
    #         if p == "D"
    #             position += [1, 0]
    #         end
    #         if p == "R"
    #             position += [0, 1]
    #         end
    #         # println("\tadded $position")
    #         s += cavern[position...]
    #     end
    #     # println("\tsum = $s")
    #     if s < lowest_risk
    #         lowest_risk = s
    #     end
    # end
    position = [1, 1]
    positions = [position]
    vals = [position...]
    risk = 0
    for s in 1:(rows + cols - 3)
        # print(s, " ")
        r, c = position
        if r == rows
            p = [0, 1]
        elseif c == cols
            p = [1, 0]
        else
            d = cavern[r + 1, c] - cavern[r, c + 1]
            if d < 0
                p = [1, 0]
            elseif d > 0
                p = [0, 1]
            else
                p = [1, 0]
            end
        end
        position += p
        push!(positions, position)
        # println("($r, $c) ", cavern[position...], " ")
        risk += cavern[position...]
    end
    # println("(10, 10) ", cavern[10, 10], " ")
    push!(positions, [rows, cols])
    risk += cavern[rows, cols]
    println("Lowest risk = $risk")
    for r in 1:rows
        display_row = ""
        for c in 1:cols
            if [r, c] in positions
                display_row *= "$(cavern[r, c])"
            else
                display_row *= " "
            end
        end
        println(display_row)
    end
end

solve("01test1.txt")
solve("01.txt")
