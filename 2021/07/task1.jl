function cost(x, p)
    return sum(abs.(x .- p))
end

function solve(input_file)
    positions = map(x->parse(Int, x), split(readline(input_file), ","))

    p_min, p_max = minimum(positions), maximum(positions)
    costs = []
    for p in p_min:p_max
        push!(costs, [p, cost(positions, p)])
    end
    fuel_cost = sort(costs, by=x->x[2])[1][2]
    println("Fuel cost: ", fuel_cost)
end

solve("01test.txt")
solve("01.txt")
