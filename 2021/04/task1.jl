function solve(input_file)
    tables_raw = Dict()
    n = 0
    draws = []
    for (i, line) in enumerate(readlines(input_file))
        # println("Reading line $i")
        if length(line) == 0
            # println("reached EOL")
            n += 1
            tables_raw[n] = []
        elseif i > 1
            # println("adding to table")
            line_split = [x for x in split(line, " ") if length(x) > 0]
            push!(tables_raw[n], map(x->parse(Int, x), line_split))
        else
            # println("saving sequence of draws")
            for d in split(line, ",")
                push!(draws, parse(Int, d))
            end
        end
    end
    tables = Dict()
    marked = Dict()
    for t in 1:n
        tables[t] = zeros(Int, 5, 5)
        marked[t] = zeros(Int, 5, 5)
        for (i, line) in enumerate(tables_raw[t])
            tables[t][i, :] = line
        end
    end

    for (i, d) in enumerate(draws)
        # println("Round $i, drawn $d")
        bingo = []
        for t in 1:n
            if d in tables[t]
                marked[t][indexin(d, tables[t])...] = 1
            end
            # display(marked[t])
            push!(bingo, any([maximum(sum(marked[t], dims=x))==5 for x in 1:2]))
        end
        if any(bingo)
            winning_table = indexin(true, bingo)[1]
            println("BINGOOOOOO on table $winning_table")
            sum_unmarked = sum(tables[winning_table][marked[winning_table] .== 0])
            println("$sum_unmarked x $d = $(sum_unmarked*d)")
            break
        end
        # println("-"^20)
    end
end

solve("01test.txt")
solve("01.txt")
