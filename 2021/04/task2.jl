function solve(input_file)
    tables_raw = Dict()
    n = 0
    draws = []
    for (i, line) in enumerate(readlines(input_file))
        # println("Reading line $i")
        if length(line) == 0
            # println("reached EOL")
            n += 1
            tables_raw[n] = []
        elseif i > 1
            # println("adding to table")
            line_split = [x for x in split(line, " ") if length(x) > 0]
            push!(tables_raw[n], map(x->parse(Int, x), line_split))
        else
            # println("saving sequence of draws")
            for d in split(line, ",")
                push!(draws, parse(Int, d))
            end
        end
    end
    tables = Dict()
    marked = Dict()
    for t in 1:n
        tables[t] = zeros(Int, 5, 5)
        marked[t] = zeros(Int, 5, 5)
        for (i, line) in enumerate(tables_raw[t])
            tables[t][i, :] = line
        end
    end

    for (i, d) in enumerate(draws)
        # println("Round $i, drawn $d")
        bingo = Dict(k=>false for k in keys(tables))
        for t in keys(tables)
            if d in tables[t]
                marked[t][indexin(d, tables[t])...] = 1
            end
            # display(marked[t])
            bingo[t] = any([maximum(sum(marked[t], dims=x))==5 for x in 1:2])
        end
        for (winning_table, tt) in filter(x->x[2] == true, bingo)
            println("($(i)) BINGO on table $winning_table")
            if length(keys(tables)) > 1
                pop!(bingo, winning_table)
                pop!(tables, winning_table)
                pop!(marked, winning_table)
                println("removed $winning_table, $(length(keys(tables))) remaining...")
            else
                println("It was the last remaining table")
                sum_unmarked = sum(tables[winning_table][marked[winning_table] .== 0])
                println("$sum_unmarked x $d = $(sum_unmarked*d)")
                return nothing
            end
        end
    end
end

solve("01test.txt")
solve("01.txt")
