function solve(input_file)
    lines = map(x->split(x, "|"), readlines(input_file))
    digits = Dict(
        0=>[1,1,1,0,1,1,1],
        1=>[0,0,1,0,0,1,0],
        2=>[1,0,1,1,1,0,1],
        3=>[1,0,1,1,0,1,1],
        4=>[0,1,1,1,0,1,0],
        5=>[1,1,0,1,0,1,1],
        6=>[1,1,0,1,1,1,1],
        7=>[1,0,1,0,0,1,0],
        8=>[1,1,1,1,1,1,1],
        9=>[1,1,1,1,0,1,1],
    )
    digit_map = Dict(2=>1, 4=>4, 3=>7, 7=>8)
    result = 0
    for (pattern, value) in lines
        pattern_list = split(strip(pattern), " ")
        pos = Dict()
        pos["a"] = (setdiff(Set(filter(x->(length(x) == 3), pattern_list)[1]), Set(filter(x->(length(x) == 2), pattern_list)[1])))
        pos["d"] = intersect(Set(filter(x->(length(x) == 4), pattern_list)[1]), intersect(map(x->Set(x), filter(x->(length(x) == 5), pattern_list))...))
        pos["c"] = [setdiff(intersect(Set(x), Set(filter(x->(length(x) == 4), pattern_list)[1])), pos["d"]) for x in filter(x->(length(x) == 5), pattern_list) if length(intersect(Set(x), Set(filter(x->(length(x) == 4), pattern_list)[1]))) == 2]
        # counts = Dict(y=>count(z->(z == y), map(w->length(w), pattern_list)) for y in unique(map(x->length(x), pattern_list)))
        # remaining_counts = filter(x->(x[2]==1), counts)
        # println("$pattern means:")
        # for p in pattern_list
        #     p_length = length(p)
        #     if p_length in keys(digit_map)
        #         p_decoded = "$(digit_map[p_length])"
        #     else
        #         p_decoded = "?"
        #     end
        #     println("- $p: $p_decoded")
        # end
        # res = count(x->(x in keys(unique_counts)), map(x->length(x), split(strip(value), " ")))
        # result += res
        # println("$value = $res")
    end
    # println("result $result")
end

solve("01test1.txt")
# solve("01test2.txt")
# solve("01.txt")
