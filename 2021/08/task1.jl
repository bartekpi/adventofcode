function solve(input_file)
    lines = map(x->split(x, "|"), readlines(input_file))
    digit_map = Dict(2=>1, 4=>4, 3=>7)
    result = 0
    for (pattern, value) in lines
        pattern_list = split(strip(pattern), " ")
        counts = Dict(y=>count(z->(z == y), map(w->length(w), pattern_list)) for y in unique(map(x->length(x), pattern_list)))
        unique_counts = filter(x->(x[2]==1), counts)
        res = count(x->(x in keys(unique_counts)), map(x->length(x), split(strip(value), " ")))
        result += res
        # println("$value = $res")
    end
    println("result $result")
end

solve("01test1.txt")
solve("01test2.txt")
solve("01.txt")
