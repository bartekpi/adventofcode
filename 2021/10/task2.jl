function solve(input_file)
    lines = readlines(input_file)

    autocomplete_score = Dict(")"=>1, "]"=>2, "}"=>3, ">"=>4)
    openings = split("([{<", "")
    closures = split(")]}>", "")
    types = Dict(
        "("=>")",
        ")"=>")",
        "["=>"]",
        "]"=>"]",
        "{"=>"}",
        "}"=>"}",
        "<"=>">",
        ">"=>">",
    )
    scores = []
    for line in lines
        order = []
        valid = true
        for (i, c) in enumerate(split(line, ""))
            # println("[$i] character: $c")
            if c in openings
                push!(order, types[c])
            elseif c == order[end]
                pop!(order)
            else
                # println("$line - Expected $(order[end]), but found $c instead")
                valid = false
                break
            end
        end
        if valid
            # println("$line - Complete by adding $(reverse(order))")
            score = 0
            # println("- Start with total score of 0")
            for c in reverse(order)
                # println("- Multiply the total score by 5 to get $(5*score), then add the value of $c ($(autocomplete_score[c])) to get a new total score of $(5*score + autocomplete_score[c])")
                score = 5*score + autocomplete_score[c]
            end
            # println("$reverse(order) - $score total points")
            push!(scores, score)
        end
    end
    println("Middle score is $(sort(scores)[Int((size(scores)[1]+1)/2)])")
end

solve("01test.txt")
solve("01.txt")
