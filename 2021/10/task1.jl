function solve(input_file)
    lines = readlines(input_file)

    syntax_score = Dict(")"=>3, "]"=>57, "}"=>1197, ">"=>25137)
    openings = split("([{<", "")
    closures = split(")]}>", "")
    types = Dict(
        "("=>")",
        ")"=>")",
        "["=>"]",
        "]"=>"]",
        "{"=>"}",
        "}"=>"}",
        "<"=>">",
        ">"=>">",
    )
    score = 0
    for line in lines
        order = []
        for (i, c) in enumerate(split(line, ""))
            if c in openings
                push!(order, types[c])
            elseif c == order[end]
                pop!(order)
            else
                # println("$line - Expected $(order[end]), but found $c instead")
                score += syntax_score[c]
                break
            end
        end
    end
    println("Total score is $score")
end

solve("01test.txt")
solve("01.txt")
