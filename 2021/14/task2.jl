function solve(input_file)
    template = readline(input_file)
    n_steps = 40
    pairs = Dict()
    for line in readlines(input_file)
        dlm = findfirst("->", line)
        if dlm != nothing
            p = strip(line[1:first(dlm)-1])
            s = strip(line[(last(dlm)+1):end])
            pairs[p] = s
        end
    end
    # println("Template: $template")
    # println(pairs)
    for s in 1:n_steps
        println("Step $s")
        template_2 = template[1]
        for i in 1:(length(template)-1)
            pp = template[i:i+1]
            # println("Pair $i: $pp so insert $(pairs[pp]) between $(pp[1]) and $(pp[2])")
            template_2 *= pairs[pp] * pp[2]
        end
        template = template_2
        # println("After step $s: $template")
    end
    counts = Dict(k=>length(findall(x->x==k, collect(template))) for k in unique(template))
    counts = sort([(k, v) for (k, v) in counts], by=x->x[2])

    println("Result is $(counts[end][2] - counts[1][2])")
end

solve("01test1.txt")
solve("01.txt")
