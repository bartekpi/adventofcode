function solve(file_name)
    nums = map(x->parse(Int, x), readlines(file_name))
    inc = 0
    for i in 2:length(nums)
        if nums[i] > nums[i-1]
            inc += 1
        end
    end
    println(inc)
end


solve("01test.txt")
solve("01.txt")
