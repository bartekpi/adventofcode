function solve(input_file)
    instructions = readlines(input_file)
    x, y, a = zeros(Int, 3)
    for line in instructions
        tokens = split(line, " ")
        direction = tokens[1]
        n = parse(Int, tokens[2])
        if direction == "forward"
            x += n
            y += n*a
        elseif direction == "up"
            a -= n
        elseif direction == "down"
            a += n
        end
    end
    println("(width $x, depth $y) = $(x*y)")
end

solve("01test.txt")
solve("01.txt")
