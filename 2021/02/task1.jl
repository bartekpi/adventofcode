function solve(input_file)
    instructions = readlines(input_file)
    x, y = 0, 0
    for line in instructions
        tokens = split(line, " ")
        direction = tokens[1]
        n = parse(Int, tokens[2])
        if direction == "forward"
            x += n
        elseif direction == "up"
            y -= n
        elseif direction == "down"
            y += n
        end
    end
    println("(width $x, depth $y) = $(x*y)")
end

solve("01test.txt")
solve("01.txt")
