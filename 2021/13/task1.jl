function solve(input_file)
    coords, folds = [], []
    for line in readlines(input_file)
        if findfirst(",", line) != nothing
            x, y = map(x->parse(Int, x), split(line, ","))
            push!(coords, [x + 1, y + 1])
        end
        if findfirst("fold along", line) != nothing
            x, y = split(split(line, " ")[end], "=")
            push!(folds, [x, parse(Int, y) + 1])
        end
    end
    coords = hcat(coords...)'
    x_min, y_min = minimum(coords, dims=1)
    x_max, y_max = maximum(coords, dims=1)

    cave = zeros(Int, x_max, y_max)'
    for (x, y) in eachrow(coords)
        cave[y, x] = 1
    end
    i = 0
    for (ax, x) in folds
        i += 1
        if ax == "y"
            if size(cave)[1] != 2*x + 1
                cave = hcat(cave, zeros(Int, 2*x - size(cave)[2] - 1, x_max))
            end
            cave = cave[1:(x-1), :] .| cave[end:-1:(x+1), :]
        end
        if ax == "x"
            if size(cave)[2] != 2*x + 1
                cave = hcat(cave, zeros(Int, y_max, 2*x - size(cave)[2] - 1))
            end
            cave = cave[:, (x-1):-1:1] .| cave[:, (x+1):end]
        end
        if i == 1
            println("Dots visible after 1st fold: $(sum(cave))")
            break
        end
    end
    println("Dots visible after $i folds: $(sum(cave))")
end

solve("01test1.txt")
solve("01.txt")
