function solve(input_file)
    points = map(x->map(x->parse(Int, x), collect(x)), readlines(input_file))
    heightmap = hcat(points...)'
    rows = size(heightmap)[1]
    cols = size(heightmap)[2]
    low_points = []
    for row in 1:rows
        for col in 1:cols
            location = heightmap[row, col]
            adjacents = []
            if row > 1
                push!(adjacents, heightmap[row - 1, col])
            end
            if row < rows
                push!(adjacents, heightmap[row + 1, col])
            end
            if col > 1
                push!(adjacents, heightmap[row, col - 1])
            end
            if col < cols
                push!(adjacents, heightmap[row, col + 1])
            end
            # println("compare $location at ($row, $col), vs $adjacents")
            if location < minimum(adjacents)
                push!(low_points, location)
            end
        end
    end
    risk_level = map(x->x+1, low_points)
    println("Sum of risk levels: $(sum(risk_level))")
end

solve("01test.txt")
solve("01.txt")
