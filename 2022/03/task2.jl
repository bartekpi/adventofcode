function solve(file_in)
    items_list = readlines(file_in)
    priorities = Dict(y => x for (x, y) in enumerate(vcat('a':'z', 'A':'Z')))
    all_shared = []
    for i in 1:length(items_list)
        if (i % 3 == 0)
            items3 = items_list[i-2:i]
            shared_items = intersect([Set(x) for x in items3]...)
            all_shared = vcat(all_shared, collect(shared_items))
            # println("$items3, $shared_items")
        end
    end
    shared_with_priority = [(x, priorities[x]) for x in all_shared]
    println("($file_in) sum = ", sum([x[2] for x in shared_with_priority]))
end

solve("01test.txt")
solve("01.txt")
