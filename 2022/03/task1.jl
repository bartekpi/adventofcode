function solve(file_in)
    items_list = readlines(file_in)
    priorities = Dict(y => x for (x, y) in enumerate(vcat('a':'z', 'A':'Z')))
    all_shared = []
    for items in items_list
        item1 = items[1:Int(length(items) / 2)]
        item2 = items[Int(length(items) / 2 + 1):end]
        shared_items = intersect(Set(item1), Set(item2))
        all_shared = vcat(all_shared, collect(shared_items))
        # println("$item1, $item2, $shared_items")
    end
    shared_with_priority = [(x, priorities[x]) for x in all_shared]
    println("($file_in) sum = ", sum([x[2] for x in shared_with_priority]))
end

solve("01test.txt")
solve("01.txt")
