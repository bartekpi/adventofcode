function parse_expr(x, expr)
    x1, op, x2 = split(expr, " ")
    vals = [
        x1 == "old" ? x : parse(Int, x1),
        x2 == "old" ? x : parse(Int, x2)
    ]
    if op == "*"
        return vals[1] * vals[2]
    else
        return vals[1] + vals[2]
    end
end

function solve(input_file)
    # parse input
    monkeys = Dict()
    for line in read(input_file, String) |> v -> split(v, "\n\n")
        els = split(line, "\n")
        monkey_no = parse(Int, split(els[1][1:end-1], " ")[2])
        monkey_items = map(x -> parse(Int, x), split(split(els[2], ":")[2], ","))
        monkey_calc = split(els[3], "= ")[2]
        monkey_div = parse(Int, split(els[4], " ")[end])
        monkey_target = map(x -> parse(Int, x), [split(els[i], " ")[end] for i in [5, 6]])
        monkeys[monkey_no] = Dict(
            "items" => monkey_items,
            "calc" => monkey_calc,
            "div" => monkey_div,
            "target" => monkey_target
        )
    end
    # println(monkeys)
    # do rounds
    n_rounds = 20
    monkey_numbers = sort(collect(keys(monkeys)))
    inspections = Dict(m => 0 for m in monkey_numbers)
    for i in 1:n_rounds
        # println("== Round $i ==")
        for m in monkey_numbers
            # println("Monkey $m")
            while !isempty(monkeys[m]["items"])
                inspections[m] += 1
                item = popfirst!(monkeys[m]["items"])
                # println("  Monkey inspects an item with a worry level of $item.")
                item = parse_expr(item, monkeys[m]["calc"])
                # println("    Worry level changes to $item.")
                item = floor(Int, item / 3)
                # println("    Monkey gets bored with item. Worry level changes to $item.")
                target_m = monkeys[m]["target"][item % monkeys[m]["div"] == 0 ? 1 : 2]
                # println("    Item with worry level $item is thrown to monkey $target_m.")
                push!(monkeys[target_m]["items"], item)
            end
        end
        # println("After round $i, the monkeys are holding items with these worry levels:")
        # for m in monkey_numbers
        #     items = join(monkeys[m]["items"], ", ")
        #     println("Monkey $m: $items")
        # end
    end
    # for m in monkey_numbers
    #     println("Monkey $m inspected items $(inspections[m]) times.")
    # end
    result = reduce(*, sort(collect(values(inspections)), rev=true)[1:2])
    println("($input_file) $result")
end

solve("01test.txt")
solve("01.txt")
