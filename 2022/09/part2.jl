sq(x) = x^2

function distance(x, y)
    reduce(+, map(sq, x .- y)) |> sqrt
end

function printmat(M)
    l = ""
    ls = []
    for r in eachrow(M)
        l = ""
        for c in r
            l *= "$c"
        end
        l *= "\n"
        push!(ls, l)
    end
    join(reverse(ls), "")
end

function debug_positions(positions)
    grid = reshape(["." for _ in 1:(30*30)], (30, 30))
    grid[15, 15] = "s"
    for j in 10:-1:1
        grid[14 .+ (map(Int, positions[j] .+ 1))...] = j == 1 ? "H" : "$(j-1)"
    end
    println(printmat(permutedims(grid)))
end

function solve(input_file)
    tails = Dict([0.0, 0.0] => 1)
    # positions = Dict("H" => [0.0, 0.0], "T" => [0.0, 0.0])
    positions = Dict(i => [0.0, 0.0] for i in 1:10)
    directions = Dict(
        "R" => [1.0, 0.0],
        "U" => [0.0, 1.0],
        "L" => [-1.0, 0.0],
        "D" => [0.0, -1.0]
    )

    # println("== Initial state ==\n$positions")
    # debug_positions(positions)

    for line in readlines(input_file)
        d, n = split(line, " ")
        direction = directions[d]
        n = parse(Int, n)
        # println("== $line ==")
        for i in 1:n
            # println("move head by 1 towards $direction")
            positions[1] = positions[1] .+ direction
            # println("Head is at $(positions[1]):")
            # debug_positions(positions)
            for j in 1:9
                dist = distance(positions[j], positions[j+1])
                front = j-1 == 0 ? "H" : "$(j+1)"
                # println("check how far knot $j is from $front ? d = $dist")
                moved = false
                m = [0., 0.]
                if dist == 2
                    m = (positions[j] .- positions[j+1]) / dist
                    positions[j+1] = positions[j+1] .+ m
                    moved = true
                elseif (dist == sqrt(5)) | (dist == sqrt(8))
                    m = sign.(positions[j] .- positions[j+1])
                    positions[j+1] = positions[j+1] .+ m
                    moved = true
                end

                # if moved
                #     println("\tmoved $(j) to $(positions[j+1]) by $m")
                #     debug_positions(positions)
                # end
            end
            if haskey(tails, positions[10])
                tails[positions[10]] += 1
            else
                tails[positions[10]] = 1
            end
        end
        # println("after considering all knots:")
        # debug_positions(positions)
        # x = readline()
        # if x == "x"
        #     return
        # end
    end
    result = length(keys(tails))
    println("($input_file) $result")
end

solve("01test.txt")
solve("01test2.txt")
solve("01.txt")
