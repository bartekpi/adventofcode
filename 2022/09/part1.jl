sq(x) = x^2

function distance(x, y)
    reduce(+, map(sq, x .- y)) |> sqrt
end

function solve(input_file)
    tails = Dict([0.0, 0.0] => 1)
    positions = Dict("H" => [0.0, 0.0], "T" => [0.0, 0.0])
    directions = Dict(
        "R" => [1.0, 0.0],
        "U" => [0.0, 1.0],
        "L" => [-1.0, 0.0],
        "D" => [0.0, -1.0]
    )
    for line in readlines(input_file)
        d, n = split(line, " ")
        direction = directions[d]
        n = parse(Int, n)
        for _ in 1:n
            positions["H"] = positions["H"] .+ direction
            m = [0.0, 0.0]
            dist = distance(values(positions)...)

            if dist == 2
                m = (positions["H"] .- positions["T"]) / dist
                positions["T"] = positions["T"] .+ m
            elseif dist == sqrt(5)
                positions["T"] = positions["H"] .- direction
            end
            if haskey(tails, positions["T"])
                tails[positions["T"]] += 1
            else
                tails[positions["T"]] = 1
            end
        end
    end
    result = length(keys(tails))
    println("($input_file) $result")
end

solve("01test.txt")
solve("01.txt")
