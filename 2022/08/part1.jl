function solve(input_file)
    inputs = read(input_file, String)
    grid = collect(inputs) |> v -> filter(x -> x != '\n', v) |> v -> map(x -> parse(Int, x), v)
    reshaped_dims = (1, 1) .* Int(sqrt(length(grid)))
    grid = reshape(grid, reshaped_dims)'
    visible = sum(reshaped_dims) + 2 * (reshaped_dims[1] - 2)
    for r in 2:(reshaped_dims[1]-1)
        for c in 2:(reshaped_dims[2]-1)
            v = grid[r, c]
            vis_top = v > maximum(grid[1:(r-1), c])
            vis_bottom = v > maximum(grid[(r+1):end, c])
            vis_left = v > maximum(grid[r, 1:(c-1)])
            vis_right = v > maximum(grid[r, (c+1):end])
            if any([vis_top, vis_bottom, vis_left, vis_right])
                visible += 1
            end
        end
    end
    println("($input_file) -> $visible")
end

solve("01test.txt")
solve("01.txt")
