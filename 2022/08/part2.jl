function solve(input_file)
    inputs = read(input_file, String)
    grid = collect(inputs) |> v -> filter(x -> x != '\n', v) |> v -> map(x -> parse(Int, x), v)
    reshaped_dims = (1, 1) .* Int(sqrt(length(grid)))
    grid = reshape(grid, reshaped_dims)'
    scenic_scores = []
    for r in 2:(reshaped_dims[1]-1)
        for c in 2:(reshaped_dims[2]-1)
            v = grid[r, c]
            vis_top = (reverse, grid[1:(r-1), c])
            vis_bottom = (identity, grid[(r+1):end, c])
            vis_left = (reverse, grid[r, 1:(c-1)])
            vis_right = (identity, grid[r, (c+1):end])

            scores = []
            for (f, vec) in [vis_top, vis_left, vis_bottom, vis_right]
                sc = 0
                # println("($r, $c) $v in $(f(vec))")
                for el in f(vec)
                    if el < v
                        sc += 1
                    else
                        sc += 1
                        break
                    end
                end
                push!(scores, sc)
            end
            # println("($r, $c) $v $scores")
            push!(scenic_scores, reduce(*, scores))
        end
    end
    result = maximum(scenic_scores)
    println("($input_file) -> $result")
end

solve("01test.txt")
solve("01.txt")
