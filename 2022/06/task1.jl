function solve(file_in)
    characters = read(file_in, String)
    # println(characters)
    # println(unique(characters))
    n = length(characters)
    result = 0
    for i in 4:n
        check = characters[i-3:i]
        # println(check)
        if length(unique(check)) == 4
            result = i
            break
        end
    end
    println("$(file_in) = $result")
end

solve("01test1.txt")
solve("01test2.txt")
solve("01test3.txt")
solve("01test4.txt")
solve("01test5.txt")
solve("01.txt")
