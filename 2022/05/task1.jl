function solve(file_in)
    stacks_all, moves = read(file_in, String) |> x -> split(x, "\n\n")

    stacks = split(stacks_all, "\n")
    max_rows = length(stacks) - 1
    crates = Dict()
    for x in stacks[end]
        if x != ' '
            c = parse(Int, x)
            col = 2 + (c - 1) * 4
            for row in 1:max_rows
                v = stacks[row][col]
                if v != ' '
                    if haskey(crates, c)
                        push!(crates[c], v)
                    else
                        crates[c] = [v]
                    end
                end
            end
        end
    end
    # println(crates)
    for move in split(moves, "\n")
        n, c_from, c_to = split(move)[[2, 4, 6]] |> x -> map(y -> parse(Int, y), x)
        for _ in 1:n
            v = popfirst!(crates[c_from])
            # println("take $v from $c_from and put on $c_to")
            crates[c_to] = vcat([v], crates[c_to])
        end
        # println(crates)
    end
    result = [crates[x][1] for x in 1:maximum(keys(crates))] |> join
    println("($file_in) $result")
end

solve("01test.txt")
solve("01.txt")
