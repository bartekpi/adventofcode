function printmat(M)
    l = ""
    ls = []
    for r in eachrow(M)
        l = ""
        for c in r
            l *= "$c"
        end
        l *= "\n"
        push!(ls, l)
    end
    join(ls, "")
end

function solve(input_file)
    cycle_increment = Dict("noop" => 1, "addx" => 2)

    x = 1
    cycles = 0
    sprite_position = [0, 1, 2]
    crt = ""
    for line in readlines(input_file)
        if line == "noop"
            instr = "noop"
            v = 0
        else
            instr = "addx"
            v = parse(Int, split(line, " ")[2])
        end
        for i in 1:cycle_increment[instr]
            cycles += 1
            # println("(begin $cycles)")
            if i == 1
                # println("(during $cycles) start executing $instr $v")
            end

            # during cycle
            pixel = ((cycles - 1) % 40 in sprite_position) ? "#" : "."
            crt *= pixel
            # println("(during $cycles) x = $x\n\t Draw pixel '$pixel' on position $(cycles-1) [checked that $((cycles - 1) % 40) in $sprite_position]")
            # println("current crt row: $crt")

            # finish cycle
            if i == cycle_increment[instr]
                x += v
                sprite_position = sprite_position .+ v
                # println("(end of $cycles) x = $x")
            end
            # println("\tsprite position: $sprite_position")
        end
    end
    println(
        "($input_file):\n",
        printmat(
            permutedims(reshape(collect(crt), (40, 6)))
        )
    )
end

solve("01test2.txt")
solve("01.txt")
