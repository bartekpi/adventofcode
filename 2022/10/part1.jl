function solve(input_file)
    cycle_increment = Dict("noop" => 1, "addx" => 2)

    x = 1
    cycles = 0
    signal_strenghts = 0
    for line in readlines(input_file)
        if line == "noop"
            instr = "noop"
            v = 0
        else
            instr = "addx"
            v = parse(Int, split(line, " ")[2])
        end
        for i in 1:cycle_increment[instr]
            cycles += 1
            if cycles in [20, 60, 100, 140, 180, 220]
                # println("(during $cycles): $instr -> $v -> x = $x")
                signal_strenghts += cycles * x
            end

            if i == cycle_increment[instr]
                x += v
            end
        end
    end
    println("($input_file) $signal_strenghts")
end

solve("01test1.txt")
solve("01test2.txt")
solve("01.txt")
