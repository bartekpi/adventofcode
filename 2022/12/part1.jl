function matrix_to_string(M)
    l = ""
    ls = []
    for r in eachrow(M)
        l = ""
        for c in r
            l *= "$c"
        end
        l *= "\n"
        push!(ls, l)
    end
    join(ls, "")
end

function input_to_matrix(input_file)
    heightmap = []
    rows, cols = (0, 0)
    for row in readlines(input_file)
        rows += 1
        cols = length(row)
        heightmap = vcat(heightmap, collect(row))
    end
    heightmap = permutedims(reshape(heightmap, (cols, rows)))
end

struct Graph
    edges::Matrix{Integer}
    vertices::Matrix{Integer}
    grid::Matrix{<:Any}
    function Graph(heightmap::Union{Matrix{Char},Matrix{Any}})
        directions = [
            ("v", [1, 0]),
            (">", [0, 1]),
            ("^", [-1, 0]),
            ("<", [0, -1])
        ]

        matrix_dims = Tuple(ones(Int, 2) * reduce(*, size(heightmap)))
        edges = zeros(Int, matrix_dims)
        vertices = zeros(Int, matrix_dims)
        grid = replace(heightmap, 'S' => 'a', 'E' => 'z')
        rows, columns = size(grid)
        for row in 1:rows
            for column in 1:columns
                position = [row, column]
                position_i = (column - 1) * rows + row
                for (_, direction) in directions
                    next_position = position .+ direction
                    position_j = (column + direction[2] - 1) * rows + row + direction[1]
                    # println("($position_i)$position -> ($position_j)$next_position:")
                    if all(next_position .> 0) & all(next_position .<= [rows, columns])
                        diff = grid[next_position...] - grid[position...]
                        # println("\tdiff: $diff")
                        if diff <= 1
                            edges[position_i, position_j] = 1
                            vertices[position_i, position_j] = 1
                        end
                    end
                end
            end
        end
        new(edges, vertices, grid)
    end
end

h(x, y) = reduce(+, map(x -> abs(x), y .- x))

function reconstruct_path(steps, finish, start)
    total_path = [finish]
    current = finish
    while current != start
        current = steps[current]
        total_path = vcat([current], total_path)
    end
    return total_path
end

function find_path_astar(graph::Graph, start::Vector{<:Integer}, finish::Vector{<:Integer})
    (rows, columns) = size(graph.grid)
    # println("looking for paths from ($start) to ($finish)")

    openSet = Set([start])
    cameFrom = Dict()
    gScore = Dict()
    fScore = Dict()
    for r in 1:rows, c in 1:columns
        gScore[[r, c]] = Inf
        fScore[[r, c]] = Inf
    end
    gScore[start] = 0
    fScore[start] = h(start, start)


    while length(openSet) > 0
        current = sort([(x, fScore[x]) for x in openSet], by = x -> x[2])[1][1]
        if current == finish
            return reconstruct_path(cameFrom, finish, start)
        end
        delete!(openSet, current)
        current_pos = (current[2] - 1) * rows + current[1]
        current_neighbours = map(x -> [x[1], x[2]], CartesianIndices(graph.grid)[findall(x -> x > 0, graph.edges[current_pos, :])])
        for neighbour in current_neighbours
            tentative_gScore = gScore[current] + 1
            if tentative_gScore < gScore[neighbour]
                cameFrom[neighbour] = current
                gScore[neighbour] = tentative_gScore
                fScore[neighbour] = tentative_gScore + h(neighbour, finish)
                if !(neighbour in openSet)
                    push!(openSet, neighbour)
                end
            end
        end
    end
    return []
end

function solve(input_file)
    heightmap = input_to_matrix(input_file)
    # println(matrix_to_string(heightmap))

    start_pos = findfirst(x -> x == 'S', heightmap) |> Tuple |> v -> [x for x in v]
    end_pos = findfirst(x -> x == 'E', heightmap) |> Tuple |> v -> [x for x in v]
    heightmap = replace(heightmap, 'S' => 'a', 'E' => 'z')
    # @show (start_pos, end_pos)

    graph = Graph(heightmap)
    result = find_path_astar(graph, start_pos, end_pos)
    println("($input_file) => $(length(result) - 1)")
end

solve("01test.txt")
solve("01.txt")
