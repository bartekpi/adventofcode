function solve(file_in)
    assignments_list = map(x -> split(x, ","), readlines(file_in))
    result = 0
    for (elf1, elf2) in assignments_list
        e1from, e1to = map(x -> parse(Int, x), split(elf1, "-"))
        e2from, e2to = map(x -> parse(Int, x), split(elf2, "-"))
        e1 = collect(e1from:e1to)
        e2 = collect(e2from:e2to)
        if (intersect(e1, e2) == e1) | (intersect(e1, e2) == e2)
            # println(e1, e2)
            result += 1
        end
    end
    println("($file_in) sum = ", result)
end

solve("01test.txt")
solve("01.txt")
