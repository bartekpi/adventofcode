function compare_items(l, r)
    println("$a vs $b")

end

function compare_items(a::Vector{<:Any}, b::Vector{<:Any})
    println("")
end

function solve(input_file)
    results = Dict()
    for (i, pair) in enumerate(read(input_file, String) |> x -> split(x, "\n\n"))
        left, right = map(x -> Meta.parse(x) |> eval, split(pair, "\n"))
        println("== Pair $i ==\n- Compare $left vs $right")
        comparisons = []
        for (l, r) in zip(left, right)
            println("\t- Compare $l vs $r")
            if any([all(isa.([l, r], t)) for t in [Array, Integer]])
                push!(comparisons, all(l .<= r))
                println("\t\t-> ", all(l .<= r))
            end

            if isa(l, Array) & (!isa(r, Array))
                push!(comparisons, all(l .<= [r]))
                println("\t\t-> ", all(l .<= [r]))
            end
            if (!isa(l, Array)) & isa(r, Array)
                push!(comparisons, all([l] .<= r))
                println("\t\t-> ", all([l] .<= r))
            end
        end
        println("comparisons: $comparisons")
        results[i] = all(comparisons) & (length(left) >= length(right))
        println("")
    end
end

solve("01test.txt")
# solve("01.txt")
