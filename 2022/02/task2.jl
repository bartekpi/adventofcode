function solve(f_in)
    list_in = map(x -> split(x, " "), readlines(f_in))

    d1 = Dict("A" => 1, "B" => 2, "C" => 3)
    d2 = Dict(1 => "A", 2 => "B", 3 => "C")
    score = 0
    for (x1, x2) in list_in
        if x2 == "X"
            res = "lose"
            pick_score = (d1[x1] + 1) % 3 + 1
            pick = d2[pick_score]
            result = 0
        elseif x2 == "Y"
            res = "draw"
            pick = x1
            pick_score = d1[x1]
            result = 3
        else
            res = "win"
            pick_score = (d1[x1] + 3) % 3 + 1
            pick = d2[pick_score]
            result = 6
        end
        score += pick_score + result
        # println("$x1 is $x2 => need to $res so $pick (sc $pick_score + $result)")
    end

    println("score = $score")
end

solve("01test.txt")
solve("01.txt")
