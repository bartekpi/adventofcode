function solve(f_in)
    list_in = map(x -> split(x, " "), readlines(f_in))

    d1 = Dict("A" => 1, "B" => 2, "C" => 3)
    d2 = Dict("X" => 1, "Y" => 2, "Z" => 3)
    score = 0
    for (x1, x2) in list_in
        if d1[x1] == d2[x2]
            result = 3
        elseif in([1, -2]).(d2[x2] - d1[x1])
            result = 6
        else
            result = 0
        end
        score += d2[x2] + result
    end

    println("score = $score")
end

solve("01test.txt")
solve("01.txt")

