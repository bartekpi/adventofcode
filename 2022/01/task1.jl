function solve(f_in)

    list_in = readlines(f_in)

    calories = Dict()
    n = 0
    for (i, c) in enumerate(list_in)
        if (i == 1) | (c == "")
            n += 1
            calories[n] = 0
        else
            calories[n] += parse(Int, c)
        end
    end
    result1 = maximum(values(calories))
    println("max calories for an elf = ", result1)

    result2 = sort(calories |> values |> collect, rev=true)[1:3] |> sum
    println("value for top 3 elfs = ", result2)
end

solve("01.txt")
