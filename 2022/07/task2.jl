function solve(input_file)
    lvl = []
    d = Dict()
    lines = filter(x -> x != "", map(strip, split(read(input_file, String), "\$")))
    for line in lines
        cmd = split(line)
        if cmd[1] == "cd"
            if cmd[2] == ".."
                pop!(lvl)
            else
                push!(lvl, cmd[2])
                k = join(lvl, "/")
                if !haskey(d, k)
                    d[k] = []
                end
            end
        end
        if cmd[1] == "ls"
            n = Int((length(cmd) - 1) / 2)
            k = join(lvl, "/")
            for i in 1:n
                s = cmd[2*i]
                if s != "dir"
                    push!(d[k], parse(Int, s))
                end
            end
        end
    end
    # println(d)
    s = Dict()
    for k in keys(d)
        s[k] = length(d[k]) > 0 ? sum(d[k]) : 0
        for k_ in [x for x in keys(d) if x != k]
            if startswith(k_, k)
                s[k] += length(d[k_]) > 0 ? sum(d[k_]) : 0
            end
        end
    end
    total_space = 70_000_000
    req_unused_space = 30_000_000
    current_unused_space = total_space - s["/"]
    for (k, v) in sort(collect(s), by=x -> x[2])
        if current_unused_space + s[k] >= req_unused_space
            println("($input_file) => $v")
            break
        end
    end
end

solve("01test.txt")
solve("01.txt")
