use log::*;

mod days;
mod utils;

fn main() {
    env_logger::builder().format_timestamp(None).init();

    info!("Day 1");
    info!(
        "part 1: {:?}",
        days::day01::part1("assets/input-day01-01.txt")
    );
    info!(
        "part 2: {:?}",
        days::day01::part2("assets/input-day01-01.txt")
    );

    info!("Day 2");
    info!(
        "part 1: {:?}",
        days::day02::part1("assets/input-day02-01.txt")
    );
    info!(
        "part 2: {:?}",
        days::day02::part2("assets/input-day02-01.txt")
    );
}
