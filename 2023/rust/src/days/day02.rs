#[allow(unused_imports)]
use log::*;

#[allow(unused_imports)]
use crate::utils;

#[allow(dead_code, unused_variables)]
pub fn part1(filename: &str) -> usize {
    warn!("Solution to part 1 not implemented yet!");
    0
}

#[allow(dead_code, unused_variables)]
pub fn part2(filename: &str) -> usize {
    warn!("Solution to part 2 not implemented yet!");
    0
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::utils::test_utils::init_logger;

    #[test]
    fn test1() {
        init_logger();
        assert_eq!(part1("assets/input_file"), 1);
    }

    // #[test]
    // fn test2() {
    //     init_logger();
    //     assert_eq!(part2("assets/input_file"), 1);
    // }
}
