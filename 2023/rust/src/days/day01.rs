#[warn(unused_imports)]
use log::*;

use crate::utils;

pub fn part1(filename: &str) -> usize {
    // version 1
    // let mut result = 0;
    // if let Ok(lines) = utils::read_input(filename) {
    //     for line in lines {
    //         let mut nums: Vec<usize> = Vec::new();
    //         for c in line.chars() {
    //             if c.is_numeric() {
    //                 nums.push(String::from(c).parse().unwrap());
    //             }
    //         }
    //         let number = nums.first().unwrap() * 10 + nums.last().unwrap();

    //         result += number;
    //     }
    // }

    // version 2
    let result = utils::read_input(filename)
        .map(|lines| {
            lines
                .iter()
                .map(|line| {
                    let nums: Vec<usize> = line
                        .chars()
                        .filter(|c| c.is_numeric())
                        .map(|c| c.to_digit(10).unwrap() as usize)
                        .collect();
                    nums.first().unwrap() * 10 + nums.last().unwrap()
                })
                .sum()
        })
        .unwrap_or(0);

    result
}

pub fn part2(filename: &str) -> usize {
    let digits_list = vec![
        (1, "one"),
        (2, "two"),
        (3, "three"),
        (4, "four"),
        (5, "five"),
        (6, "six"),
        (7, "seven"),
        (8, "eight"),
        (9, "nine"),
    ];
    let result = utils::read_input(filename)
        .map(|lines| {
            lines
                .iter()
                .map(|line| {
                    let mut nums: Vec<usize> = Vec::new();
                    let mut pos: Vec<usize> = Vec::new();

                    for (i, c) in line.chars().enumerate() {
                        if c.is_numeric() {
                            nums.push(c.to_digit(10).unwrap() as usize);
                            pos.push(i);
                        }
                    }

                    for (n, w) in &digits_list {
                        for p in line.match_indices(w) {
                            pos.push(p.0);
                            nums.push(*n);
                        }
                    }
                    let first_val = pos.iter().zip(nums.iter()).min().unwrap().1;
                    let last_val = pos.iter().zip(nums.iter()).max().unwrap().1;

                    first_val * 10 + last_val
                })
                .sum()
        })
        .unwrap_or(0);

    result
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::utils::test_utils::init_logger;

    #[test]
    fn test1() {
        init_logger();
        assert_eq!(part1("assets/input-day01-00-test01.txt"), 142);
    }

    #[test]
    fn test2() {
        init_logger();
        assert_eq!(part2("assets/input-day01-00-test02.txt"), 281);
    }
}
