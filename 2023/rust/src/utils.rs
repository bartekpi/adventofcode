use std::fs::read_to_string;

#[derive(Debug, PartialEq)]
pub enum InputError {
    FileNotFound,
    Other(String),
}

pub fn read_input(filename: &str) -> Result<Vec<String>, InputError> {
    match read_to_string(filename) {
        Err(e) => match e.kind() {
            std::io::ErrorKind::NotFound => Err(InputError::FileNotFound),
            _ => Err(InputError::Other(e.to_string())),
        },
        Ok(content) => Ok(content.lines().map(String::from).collect()),
    }
}

#[cfg(test)]
pub mod test_utils {
    use super::*;
    use std::sync::Once;

    static INIT: Once = Once::new();

    pub fn init_logger() {
        INIT.call_once(|| {
            env_logger::builder().format_timestamp(None).init();
        });
    }

    #[test]
    fn test_read_file() {
        let expected = Ok(vec!["a".into(), "b".into()]);
        let result = read_input("assets/test-read_input.txt");
        assert_eq!(result, expected);
    }

    #[test]
    fn test_file_doesnt_exist() {
        let res = read_input("assets/file-does-not-exist.txt");
        assert_eq!(res, Err(InputError::FileNotFound));
    }
}
