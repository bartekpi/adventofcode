using Test

function solve(input)
    bag = Dict(
        "red" => 12,
        "green" => 13,
        "blue" => 14
    )
    cubes = collect(keys(bag))

    games_powers = []
    for game in input
        _, game_string = split(game, ":")
        # println(game_id_string)
        game_cubes = Dict(c => 0 for c in cubes)
        for game_set in split(game_string, ";")
            # println(game_set)
            game_set_cubes = Dict((split(strip(s), " ") |> x -> x[2] => parse(Int, x[1])) for s in split(game_set, ","))
            # println(game_set_cubes)
            for c in keys(game_set_cubes)
                #     game_cubes[c] += in(c, keys(game_set_cubes)) ? game_set_cubes[c] : 0
                if game_set_cubes[c] > game_cubes[c]
                    game_cubes[c] = game_set_cubes[c]
                end
            end
        end
        # println(game_cubes)
        push!(games_powers, prod(values(game_cubes)))
    end
    # println("powers: ", games_powers)
    "$(sum(games_powers))"
end

if abspath(PROGRAM_FILE) == @__FILE__
    if length(ARGS) == 1
        if !ispath(ARGS[1])
            error("File $(ARGS[1]) does not exist")
        end
        input = readlines(ARGS[1])
        println(solve(input))
    else
        @testset "Part 2 test" begin
            input_raw = """Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
        Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
        Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
        Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
        Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"""
            input = split(input_raw, "\n")

            @test solve(input) == "2286"
        end
    end
end
