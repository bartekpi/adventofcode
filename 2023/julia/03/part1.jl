using Test

function solve(input)
    #== things:
    find what symbols are available
    find symbols and their positions in the string
    find numbers and their positions in the string
    calculate distances between numbers and symbols
    ==#

    # technically this should be transposed
    # but let's keep it this way because later on we're iterating through items
    # and Julia goes by column so otherwise we'd have to transpose
    input_matrix = hcat([collect(r) for r in split(input, "\n")]...) # |> permutedims
    rows, cols = size(input_matrix)

    numbers = Vector{Tuple{Int,Vector{Vector{Int}}}}()
    number = []
    symbols = Vector{Tuple{Char,Vector{Int}}}()
    for (i, char) in enumerate(input_matrix)
        row, col = ((i - 1) % rows) + 1, ceil(Int, i / cols)

        if !(isnumeric(char) | in(char, ['.', '\n']))
            push!(symbols, (char, [row, col]))
        end

        if isnumeric(char)
            push!(number, [char, [row, col]])
        elseif length(number) != 0
            number_int = parse(Int, join([x[1] for x in number]))
            number_pos = Vector{Vector{Int}}([x[2] for x in number])
            push!(numbers, (number_int, number_pos))
            number = []
        end

    end
    valid_numbers = []
    for (n, positions) in numbers
        dmin = Inf
        for (_, symbol_position) in symbols
            for position in positions
                d = sqrt(sum((symbol_position .- position) .^ 2))
                if d < dmin
                    dmin = d
                end
            end
        end
        if dmin <= sqrt(2)
            push!(valid_numbers, n)
        end
    end
    "$(sum(valid_numbers))"
end

if abspath(PROGRAM_FILE) == @__FILE__
    if length(ARGS) == 1
        if !ispath(ARGS[1])
            error("File $(ARGS[1]) does not exist")
        end
        input = read(ARGS[1], String)
        println(solve(input))
    else
        @testset "Part 1 test" begin
            input = raw"""467..114..
    ...*......
    ..35..633.
    ......#...
    617*......
    .....+.58.
    ..592.....
    ......755.
    ...$.*....
    .664.598.."""

            @test solve(input) == "4361"
        end
    end
end
