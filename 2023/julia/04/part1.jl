using Test

function solve(input)
    points = []
    for cards in input
        _, numbers = split(cards, ":")
        winning_numbers_str, my_numbers_str = split(numbers, "|") |> x -> map(split, x)
        winning_numbers = [parse(Int, x) for x in winning_numbers_str if length(x) > 0]
        my_numbers = [parse(Int, x) for x in my_numbers_str if length(x) > 0]
        p = length(intersect(my_numbers, winning_numbers))
        push!(points, p > 0 ? 2^(p - 1) : 0)
    end
    "$(sum(points))"
end

if abspath(PROGRAM_FILE) == @__FILE__
    if length(ARGS) == 1
        if !ispath(ARGS[1])
            error("File $(ARGS[1]) does not exist")
        end
        input = readlines(ARGS[1])
        println(solve(input))
    else
        @testset "Part 1 test" begin
            input_raw = """Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"""

            input = split(input_raw, "\n")

            @test solve(input) == "13"
        end
    end
end
