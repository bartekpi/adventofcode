using Test

function solve(input)

end

if abspath(PROGRAM_FILE) == @__FILE__
    if length(ARGS) == 1
        if !ispath(ARGS[1])
            error("File $(ARGS[1]) does not exist")
        end
        input = readlines(ARGS[1])
        println(solve(input))
    else
        @testset "Part 1 test" begin
            input_raw = """"""
            input = split(input_raw, "\n")
            @test solve(input) == ""
        end
    end
end
