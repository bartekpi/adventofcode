testinput = """1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet"""

# part 1
readlines("input.txt") |> 
    x -> map(y -> filter(isnumeric, y), x) |> 
    x -> map(y -> parse(Int64, y[1]*y[end]), x) |> sum

# part 2
testinput = """two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen"""

digits_list = [
    ("one", 1),
    ("two", 2),
    ("three", 3),
    ("four", 4),
    ("five", 5),
    ("six", 6),
    ("seven", 7),
    ("eight", 8),
    ("nine", 9),
]
vals = readlines("input.txt")
# vals = testinput |> x -> split(x, "\n")
vals_out = []
for (i, v) in enumerate(vals)
    f_all =[(findall(w, v), d) for (w, d) in digits_list if findfirst(w, v) !== nothing] 
    vals_list = collect(v)
    for (rs, d) in f_all
        for r in rs
            vals_list[r[1]] = Char('0' + d)
        end
    end
    push!(vals_out, join(vals_list))
end
vals_out |>
    x -> map(y -> filter(isnumeric, y), x) |> 
    x -> map(y -> parse(Int64, y[1]*y[end]), x) |> sum

for (v, vin) in zip(vals_out, vals)
    y = []
    for c in v
        if isnumeric(c)
            push!(y, parse(Int64, c))
        end
    end
    n = 10*y[1] + y[end]
    l = "\"$vin\",$n"
    println(l)
end
