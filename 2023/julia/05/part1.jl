using Test

function solve(input)
    input_list = split(input, "\n\n")
    seeds = map(x -> parse(Int, x), split(input_list[1], " ")[2:end])
    map_chain = ["seed"]
    maps = Dict()
    for input_map in input_list[2:end]
        # println(input_map, "\n==\n")
        input_map_split = split(input_map, "\n")
        map_key = split(input_map_split[1], " ")[1]
        map_source, map_destination = split(map_key, "-to-")
        # maps[map_key] = Dict(map_source => [], map_destination => [])
        maps[map_key] = []
        push!(map_chain, map_destination)
        for ranges in input_map_split[2:end]
            # @show ranges 
            start_destination, start_source, range = map(x -> parse(Int, x), split(ranges, " "))
            push!(maps[map_key], [start_destination:(start_destination+range-1), start_source:(start_source+range-1)])

            # 
            # start_destination, start_source, range = map(x -> parse(Int, x), split(ranges, " "))
            @show map_key, start_destination, start_source, range
            # maps[map_key][map_source] = vcat(maps[map_key][map_source], collect(start_source:(start_source+range-1)))
            # maps[map_key][map_destination] = vcat(maps[map_key][map_destination], collect(start_destination:(start_destination+range-1)))
        end
        # @show map_source, map_destination, maps[map_key]

        # for (s, d) in zip(maps[map_key][map_source], maps[map_key][map_destination])
        #     println("$s $d")
        # end
    end
    println("loaded maps")
    # @show map_chain
    # @show maps
    # locations = []
    for seed_number in seeds
        chain_n = []
        push!(chain_n, seed_number)
        # seed-to-soil
        @show maps["seed-to-soil"]
        for sources in maps["seed-to-soil"]
            @show sources, seed_number in sources[2]
        end
        #     seed_id = findfirst(x -> x == seed_number, maps["seed-to-soil"]["seed"])
        #     soil_number = seed_id === nothing ? seed_number : maps["seed-to-soil"]["soil"][seed_id]
        #     push!(chain_n, soil_number)
        #     # soil-to-fertilizer
        #     soil_id = findfirst(x -> x == soil_number, maps["soil-to-fertilizer"]["soil"])
        #     fertilizer_number = soil_id === nothing ? soil_number : maps["soil-to-fertilizer"]["fertilizer"][soil_id]
        #     push!(chain_n, fertilizer_number)
        #     # fertilizer-to-water
        #     fertilizer_id = findfirst(x -> x == fertilizer_number, maps["fertilizer-to-water"]["fertilizer"])
        #     water_number = fertilizer_id === nothing ? fertilizer_number : maps["fertilizer-to-water"]["water"][fertilizer_id]
        #     push!(chain_n, water_number)
        #     # water-to-light
        #     water_id = findfirst(x -> x == water_number, maps["water-to-light"]["water"])
        #     light_number = water_id === nothing ? water_number : maps["water-to-light"]["light"][water_id]
        #     push!(chain_n, light_number)
        #     prev_number = light_number
        #     # light-to-temperature
        #     prev_id = findfirst(x -> x == prev_number, maps["light-to-temperature"]["light"])
        #     next_number = prev_id === nothing ? prev_number : maps["light-to-temperature"]["temperature"][prev_id]
        #     push!(chain_n, next_number)
        #     prev_number = next_number
        #     prev_cat = "temperature"
        #     next_cat = "humidity"
        #     # temperature-to-humidity
        #     prev_id = findfirst(x -> x == prev_number, maps["$prev_cat-to-$next_cat"][prev_cat])
        #     next_number = prev_id === nothing ? prev_number : maps["$prev_cat-to-$next_cat"][next_cat][prev_id]
        #     push!(chain_n, next_number)
        #     prev_number = next_number
        #     prev_cat = next_cat
        #     next_cat = "location"
        #     # humidity-to-location
        #     prev_id = findfirst(x -> x == prev_number, maps["$prev_cat-to-$next_cat"][prev_cat])
        #     next_number = prev_id === nothing ? prev_number : maps["$prev_cat-to-$next_cat"][next_cat][prev_id]
        #     push!(chain_n, next_number)
        #     push!(locations, next_number)
        println(join([("$c: $v") for (c, v) in zip(map_chain, chain_n)], ", "))
    end

    # "$(minimum(locations))"
    ""
end

if abspath(PROGRAM_FILE) == @__FILE__
    if length(ARGS) == 1
        if !ispath(ARGS[1])
            error("File $(ARGS[1]) does not exist")
        end
        input = read(ARGS[1], String)
        println(solve(input))
    else
        @testset "Part 1 test" begin
            input = """seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4"""
            @test solve(input) == "35"
        end
    end
end
