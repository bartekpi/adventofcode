import sys
from itertools import product


def input_file(f_in):
    with open(f'{f_in}.txt', 'r') as f:
        infile = f.read()
    result = map(int, infile.replace('\n', '').split(','))
    return result


def operate(opcode, *args):
    if opcode == 1:
        res = 0
        for arg in args:
            res += arg
        return res
    if opcode == 2:
        res = 1
        for arg in args:
            res *= arg
        return res


def get_args(inlist, modes, i, n=3):
    args = []
    for j, mode in enumerate(modes[:n]):
        if mode == '0':
            args.append(inlist[inlist[i+j+1]])
        elif mode == '1':
            args.append(inlist[i+j+1])
    return args


def main(f):
    inlist = list(input_file(f))
    i = 0
    while inlist[i] != 99:
        instruction = '{:05d}'.format(inlist[i])
        opcode = int(instruction[-2:])
        modes = list(reversed(instruction[:-2]))
        if opcode in [3, 4]:
            if opcode == 3:
                val = int(input('provide: '))
                target = inlist[i+1]
            if opcode == 4:
                if '1' in instruction[:-2]:
                    print(inlist[i+1])
                else:
                    print(inlist[inlist[i+1]])
            step = 2
        elif opcode in [1, 2]:
            args = get_args(inlist, modes, i)
            target = inlist[i+3]
            val = operate(opcode, *args[:2])
            step = 4
        elif opcode == 5:
            args = get_args(inlist, modes, i, 2)
            if args[0] != 0:
                step = args[1]
            else:
                step = 3
        elif opcode == 6:
            args = get_args(inlist, modes, i, 2)
            if args[0] == 0:
                step = args[1]
            else:
                step = 3
        elif opcode == 7:
            args = get_args(inlist, modes, i)
            target = args[2]
            val = int(args[0] < args[1])
            step = 4
        elif opcode == 8:
            args = get_args(inlist, modes, i)
            target = args[2]
            val = int(args[0] == args[1])
            step = 4
        else:
            print(instruction)
            break
        inlist[target] = val
        print(i)
        i += step
        print(i)

if __name__ == '__main__':
    main(sys.argv[1])
