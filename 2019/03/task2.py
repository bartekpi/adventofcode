import sys


def input_file(f_in):
    with open(f'{f_in}.txt', 'r') as f:
        infile = f.read()
    #result = map(int, infile.replace('\n', '').split(','))
    result = infile.split('\n')
    return result


xc = {
    'R': (1, 0),
    'L': (-1, 0),
    'U': (0, 1),
    'D': (0, -1),
}

def main(f):
    lst = [x for x in input_file(f) if x != '']
    #print(lst)
    paths =  []
    for i, path in enumerate(lst):
        xmin, xmax, ymin, ymax = 0, 0, 0, 0
        paths.append([(0, 0, 0, 0)])
        t = 0
        for coords in path.split(','):
            x, d = xc[coords[0].upper()], int(coords[1:])
            x_, y_ = x
            for j in range(abs(d*sum(x))):
                t += 1
                lastx, lasty, _t0, _t1 = paths[i][-1]
                #print('current point:', paths[i][-1])
                d_ = sum(x)
                #print('adding:', d_)
                if x[0] == 0:
                    paths[i].append((lastx, lasty+d_, j+1, t))
                else:
                    paths[i].append((lastx+d_, lasty, j+1, t))
    points = [[(x, y) for x, y, z, t in path] for path in paths]
    points_common = set(set(points[0]) & set(points[1])) - set([(0, 0)])
    points = []
    for x, y in points_common:
        for path in paths:
            for x_, y_, d_, t_ in path:
                if (x, y) == (x_, y_):
                    points.append((x_, y_, d_, t_))
    #print(paths)
    #print(points)
    points_sum = {}
    for x, y, z, t in points:
        if (x, y) not in points_sum:
            points_sum[(x, y)] = 0
        points_sum[(x, y)] += t
    print(min(points_sum.values()))
    #d_min = min(points_sum.values())
    #for (x, y), d in points_sum.items():
    #    if d == d_min:
    #        print(x, y, d, ':', abs(x) + abs(y))

if __name__ == '__main__':
    main(sys.argv[1])

