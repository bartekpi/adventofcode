import sys


def input_file(f_in):
    with open(f'{f_in}.txt', 'r') as f:
        infile = f.read()
    #result = map(int, infile.replace('\n', '').split(','))
    result = infile.split('\n')
    return result


xc = {
    'R': (1, 0),
    'L': (-1, 0),
    'U': (0, 1),
    'D': (0, -1),
}

def main(f):
    lst = [x for x in input_file(f) if x != '']
    #print(lst)
    paths =  []
    for i, path in enumerate(lst):
        xmin, xmax, ymin, ymax = 0, 0, 0, 0
        paths.append([(0, 0)])
        for coords in path.split(','):
            x, d = xc[coords[0].upper()], int(coords[1:])
            x_, y_ = x
            for j in range(abs(d*sum(x))):
                lastx, lasty = paths[i][-1]
                #print('current point:', paths[i][-1])
                d_ = sum(x)
                #print('adding:', d_)
                if x[0] == 0:
                    paths[i].append((lastx, lasty+d_))
                else:
                    paths[i].append((lastx+d_, lasty))
        
    points = set(set(paths[0]) & set(paths[1])) - set([(0, 0)])
    #print(paths[0])
    points = [(abs(x), abs(y)) for x, y in points]
    print(points)
    print(min([sum(x) for x in points]))

if __name__ == '__main__':
    main(sys.argv[1])

