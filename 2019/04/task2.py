import sys
from collections import defaultdict

def input_file(f_in):
    with open(f'{f_in}.txt', 'r') as f:
        infile = f.read()
    #result = map(int, infile.replace('\n', '').split(','))
    result = infile.split('\n')
    return result


def analyse_one(string):
    d = dict(two_adjacent=False, never_decrease=True, no_three_adjacent=True)
    count_multiples = defaultdict(int)
    # print('->', string)
    for i, s in enumerate(string):
        # print('  ', f'{i}:', s)
        if i == 0:
            pass
        else:
            if string[i-1] == s:
                d['two_adjacent'] = True
                # print('    ', s, 'two adjacent')
                count_multiples[s] += 1
                if i > 1:
                    if string[i-2] == string[i-1]:
                        d['no_three_adjacent'] = False
                        # print('    ', s, 'violates no three adjacent')

            if s < string[i-1]:
                d['decrease'] = False
                # print('    ', s, 'starts to decrease')
                break
    # print(d, count_multiples)
    if (not d['no_three_adjacent']) and (1 in count_multiples.values()):
        d['no_three_adjacent'] = True
    # print(d)
    return all(d.values())
                

def main():
    result = []
    range_min = 357253
    range_max = 892942
    range_all = range(range_min, range_max+1)
    # range_all = [112233, 123444, 111122]
    for item in range_all:
        result.append(analyse_one(str(item)))
    # print(result)
    print(len(result), sum(result))
if __name__ == '__main__':
    main()
    #main(sys.argv[1])

