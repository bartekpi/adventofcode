import sys


def input_file(f_in):
    with open(f'{f_in}.txt', 'r') as f:
        infile = f.read()
    #result = map(int, infile.replace('\n', '').split(','))
    result = infile.split('\n')
    return result


def analyse_one(string):
    d = dict(two_adjacent=False, never_decrease=True, no_three_adjacent=True)
    for i, s in enumerate(string):
        if i == 0:
            pass
        else:
            if string[i-1] == s:
                d['two_adjacent'] = True

            if s < string[i-1]:
                d['decrease'] = False
                break

    return all(d.values())
                

def main():
    result = []
    range_min = 357253
    range_max = 892942
    range_all = range(range_min, range_max+1)
    #range_all = [111111, 223450, 123789]
    for item in range_all:
        result.append(analyse_one(str(item)))
    # print(result)
    print(len(result), sum(result))
if __name__ == '__main__':
    main()
    #main(sys.argv[1])

