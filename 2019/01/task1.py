import sys


def input_file(f_in):
    with open(f'{f_in}.txt', 'r') as f:
        infile = f.read()
    result = map(int, infile.split())
    return result


def main(f):
    s = 0
    inlist = input_file(f)
    for x in inlist:
        s += x // 3 - 2
    print(s)


if __name__ == '__main__':
    try:
        if sys.argv[1] in ['00', '01', '02']:
            main(sys.argv[1])
    except IndexError:
        print('args!')
