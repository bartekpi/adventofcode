"""Credit to this guy:
    https://github.com/mjpieters/adventofcode/blob/master/2019/Day%2006.ipynb
"""

from collections import deque
import sys


def input_file(f_in):
    with open(f'{f_in}.txt', 'r') as f:
        infile = f.read()
    result = filter(lambda x: x != '', infile.split('\n'))
    return result


def main(f):
    inlist = [x.split(')') for x in input_file(f)]

    graph = {}
    for x, y in inlist:
        graph.setdefault(x, set()).add(y)

    stack = deque([('COM', 0, None)])
    i = 0
    s = 0
    while stack:
        body, depth, orbits = stack.pop()
        stack += ((b, depth + 1, body) for b in graph.get(body, ()))
        i += 1
        s += depth
    print(s)


if __name__ == '__main__':
    main(sys.argv[1])
