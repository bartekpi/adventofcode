"""Credit to this guy:
    https://github.com/mjpieters/adventofcode/blob/master/2019/Day%2006.ipynb
"""
from __future__ import annotations
from itertools import islice

from collections import deque
from typing import Dict, Iterable, Mapping, Optional, Set, Tuple


DGraph = Mapping[str, Set[str]]

def read_orbits(lines: Iterable[str]) -> DGraph:
    graph: Dict[str, Set[str]] = {}
    for line in lines:
        center, _, body = line.strip().partition(')')
        graph.setdefault(center, set()).add(body)
    return graph


def walk(graph: DGraph, root: str = 'COM') -> Iterable[Tuple[str, int, Optional[str]]]:
    """Walk a graph, yielding (name, depth, orbits) tuples

    name is a body in the graph, depth the number of steps it is removed from the root
    and orbits the name of the body it is orbiting around.

    """
    stack = deque([(root, 0, None)])
    while stack:
        body, depth, orbits = stack.pop()
        yield (body, depth, orbits)
        stack += ((b, depth + 1, body) for b in graph.get(body, ()))

def total_direct_and_indirect_orbits(graph: DGraph) -> int:
    return sum(d for _, d, _ in walk(graph))


def minimum_orbital_transfers(
    graph: DGraph,
    from_: str = 'YOU',
    to: str = 'SAN',
    _graphcb: Optional[Callable[str]] = None
) -> int:
    # (d)epth is for orbiting (b)ody, so for (o)rbited, depth is d - 1
    orbiting: Dict[str, Tuple[str, int]] = ({b: (o, d - 1) for b, d, o in walk(graph) if o is not None})

    def traverse(pair: Tuple[str, int]) -> Iterator[Tuple[str, int]]:
        """Traverse orbiting bodies inwards"""
        while pair:
            if _graphcb: _graphcb(pair[0])
            yield pair
            pair = orbiting.get(pair[0])

    traversals = traverse(orbiting[from_]), traverse(orbiting[to])
    positions = [next(t) for t in traversals]
    start_depths = [d for _, d in positions]

    while positions[0] != positions[1]:
        # if not equal, only traverse the furtherst
        if positions[0][1] > positions[1][1]:
            positions[0] = next(traversals[0])
        elif positions[0][1] < positions[1][1]:
            positions[1] = next(traversals[1])
        else:
            positions = [next(t) for t in traversals]
    common_depth = positions[0][1]
    return sum(d - common_depth for d in start_depths)


if __name__ == '__main__':
    part1_testinput = """\
    COM)B
    B)C
    C)D
    D)E
    E)F
    B)G
    G)H
    D)I
    E)J
    J)K
    K)L
    """.splitlines()
    part2_testinput = part1_testinput + ["K)YOU", "I)SAN"]
    part2_testorbits = read_orbits(part2_testinput)
    assert minimum_orbital_transfers(part2_testorbits) == 4
    with open('01.txt', 'r') as f:
        data = f.read()
    orbits = read_orbits(data.splitlines())
    print("Part 2:", minimum_orbital_transfers(orbits))
