import sys
from collections import Counter


def input_file(f_in):
    with open(f'{f_in}.txt', 'r') as f:
        infile = f.read()
    result = map(int, infile.replace('\n', ''))
    return result


def main(f, dims):
    inlist = list(input_file(f))
    n, k = map(int, dims.split('x'))

    i = 0
    outlist = []
    while i < len(inlist):
        r = []
        for k_ in range(k):
            c = []
            for n_ in range(n):
                c.append(inlist[i])
                i += 1
            r.append(c)
        outlist.append(r)
    # print(outlist)

    min_num_zeros = 99
    min_i = 0
    for i, layer in enumerate(outlist):
        num_zeros = sum([Counter(x)[0] for x in layer])
        if num_zeros < min_num_zeros:
            min_num_zeros = num_zeros
            min_i = i
            min_layer = layer
    # print(min_num_zeros, min_i, min_layer)
    counter = Counter([item for sublist in min_layer for item in sublist])
    x, y = counter[1], counter[2]
    print(x*y)




if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])
