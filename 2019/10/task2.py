import sys
from itertools import product
from collections import Counter, OrderedDict
from pprint import pprint


def input_file(f_in):
    with open(f'{f_in}.txt', 'r') as f:
        infile = f.read()

    result = map(list, infile.split('\n'))
    return result


def lineparms(A, B):
    a = (B[1] - A[1])/(B[0] - A[0])
    b = B[1] - B[0]*(B[1] - A[1])/(B[0] - A[0])
    return round(a, 9), round(b, 9)


def main(f):
    offset_x, offset_y = (20, 19)
    # offset_x, offset_y = (11, 13)
    inlist = list(input_file(f))

    points = []
    for i, row in enumerate(inlist):
        for j, col in enumerate(row):
            # print(col, j, i)
            if col == '#':
                points.append((j - offset_x, i - offset_y))


    lines = {}
    for a, b in product(points, points):
        if (a != b) & (a == (0, 0)):
            # print(a, b)
            try:
                parms = (a[0] > b[0], lineparms(a, b))
            except ZeroDivisionError:
                if b[1] - a[1] > 0:
                    parms = (None, (None, 0))
                else:
                    parms = (None, (0, None))
            # print(parms)
            if a not in lines:
                lines[a] = [(parms, b)]
            else:
                lines[a].append((parms, b))

    max_key = (0, 0)
    d = dict(Counter(lines[max_key]))
    # print(d)
    move = {}
    move[0] = {k: (k[1][0]**2 + k[1][1]**2)**.5 for k, v in d.items() if k[0] == (None, (0, None))}
    move[1] = {k: (k[1][0]**2 + k[1][1]**2)**.5 for k, v in d.items() if k[0][0] == False}
    move[2] = {k: (k[1][0]**2 + k[1][1]**2)**.5 for k, v in d.items() if k[0] == (None, (None, 0))}
    move[3] = {k: (k[1][0]**2 + k[1][1]**2)**.5 for k, v in d.items() if k[0][0] == True}

    for i in range(4):
        move[i] = {k[0]: [(move[i][b], b[1]) for b in move[i].keys() if k[0] == b[0]] for k, v in move[i].items()}

    key_move_2 = list(move[2].keys())[0]
    move[2][key_move_2] = sorted(move[2][key_move_2], reverse=True)
    # pprint(move)
    i = 0
    j = 0
    while j < 4:
        for k in sorted(move[j].keys()):
            # print(f'step {i+1} removed last from', move[j][k])
            v = move[j][k].pop()[1]
            v_offset = (v[0] + offset_x, v[1] + offset_y)
            # if i in [0,1,2,3,9,10,19,20,49,50,99,100,198,199,200]:
            if i in [199]:
                print(f'({i+1})[{j}] removed point {v} which is actually {v_offset}')
                result = 100*v_offset[0] + v_offset[1]
                print(f'result {result}')
                # v_ = input()
                return None
            i += 1
        j += 1


if __name__ == '__main__':
    main(sys.argv[1])
