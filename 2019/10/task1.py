import sys
from itertools import product


def input_file(f_in):
    with open(f'{f_in}.txt', 'r') as f:
        infile = f.read()

    result = map(list, infile.split('\n'))
    return result


def lineparms(A, B):
    a = (B[1] - A[1])/(B[0] - A[0])
    b = B[1] - B[0]*(B[1] - A[1])/(B[0] - A[0])
    return round(a, 9), round(b, 9)


def main(f):
    inlist = list(input_file(f))
    points = []
    for i, row in enumerate(inlist):
        for j, col in enumerate(row):
            # print(col, j, i)
            if col == '#':
                points.append((j, i))
    lines = {}
    for a, b in product(points, points):
        if a != b:
            # print(a, b)
            try:
                parms = (a[0] > b[0], lineparms(a, b))
            except ZeroDivisionError:
                if b[1] - a[1] > 0:
                    parms = (None, None, 0)
                else:
                    parms = (None, 0, None)
            # print(parms)
            if a not in lines:
                lines[a] = [parms]
            else:
                lines[a].append(parms)
    result = {}
    for k in lines:
        result[k] = len(set(lines[k]))

    max_key = sorted(result, key=result.get)[-1]
    print(max_key, result[max_key])


if __name__ == '__main__':
    main(sys.argv[1])
