import sys


def input_file(f_in):
    with open(f'{f_in}.txt', 'r') as f:
        infile = f.read()
    result = map(int, infile.replace('\n', '').split(','))
    return result


def operate(opcode, *args):
    if opcode == 1:
        res = 0
        for arg in args:
            res += arg
        return res
    if opcode == 2:
        res = 1
        for arg in args:
            res *= arg
        return res


def main(f):
    l = list(input_file(f))
    i = 0
    l[1] = 12
    l[2] = 2
    while l[i] != 99:
        l[l[i+3]] = operate(l[i], l[l[i+1]], l[l[i+2]])
        i += 4
    print(l[0])

if __name__ == '__main__':
    main(sys.argv[1])
