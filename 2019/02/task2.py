import sys
from itertools import product


def input_file(f_in):
    with open(f'{f_in}.txt', 'r') as f:
        infile = f.read()
    result = map(int, infile.replace('\n', '').split(','))
    return result


def operate(opcode, *args):
    if opcode == 1:
        res = 0
        for arg in args:
            res += arg
        return res
    if opcode == 2:
        res = 1
        for arg in args:
            res *= arg
        return res


def main(f):
    for p in product(range(100), range(100)):
        l = list(input_file(f))
        i = 0
        l[1] = p[0]
        l[2] = p[1]
        while l[i] != 99:
            l[l[i+3]] = operate(l[i], l[l[i+1]], l[l[i+2]])
            i += 4
        if l[0] == 19690720:
            print(100*p[0] + p[1])
            break

if __name__ == '__main__':
    main(sys.argv[1])
