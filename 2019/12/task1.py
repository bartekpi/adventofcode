import sys
from itertools import product
from collections import defaultdict
from pprint import pprint


def input_file(f_in):
    with open(f'{f_in}.txt', 'r') as f:
        infile = f.read()

    result = infile.split('\n')
    return result


def main(f):
    inlist = [x.split(',') for x in input_file(f) if x != '']
    n = len(inlist)
    axes = ['x', 'y', 'z']
    pos = [[{
        'x': int(el[0].split('=')[1]),
        'y': int(el[1].split('=')[1]),
        'z': int(el[2].split('=')[1][:-1])
        } for el in inlist]]
    vel = [[{'x': 0, 'y': 0, 'z': 0} for i in range(n)]]
    print('step 0')
    # print(pos[0])
    # print(vel[0])
    i = 1
    while i <= 100:
        print('========')
        print(pos[-1])
        print(vel[-1])
        print('========')
        dpos = {}
        for k in range(n):
            dpos[k] = defaultdict(int)
        for p0, p1 in product(range(n), range(n)):
            # print(a, b)
            for ax in axes:
                dpos[p0][ax] += (int(pos[i-1][p1][ax] > pos[i-1][p0][ax])
                                 - int(pos[i-1][p1][ax] < pos[i-1][p0][ax]))
        dpos = {k: dict(v) for k, v in dpos.items()}

        print('-'*10)
        for x in range(n):
            print('\ttake', pos[i-1][x])
            print('\tadd ', dpos[x])
            print('\t--')
        print('-'*10)
        newpos = [{ax: pos[i-1][x][ax] + dpos[x][ax] for ax in axes} for x in range(n)]

        pprint(newpos)
        # return
        pos.append(newpos)
        vel.append(dpos)

        print('after', i, 'step' if i == 1 else 'steps')
        print('position:\n', pos[-1])
        print('velocity:\n', vel[-1])
        print()
        # return
        vvv = input()
        i += 1

if __name__ == '__main__':
    main(sys.argv[1])
