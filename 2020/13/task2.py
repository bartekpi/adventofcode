with open("01.txt", "r") as f:
    input_list = [x.strip() for x in f.readlines()]


buses = [(i+1, int(x)) for i, x in enumerate(input_list[1].split(","))
         if x != "x"]

ts = buses[0][1] - 1
ts = 100000000000010
n = 0
while True:
    remainders = [(b - (ts % b), b) for i, b in buses]
    ar = sorted(remainders, key=lambda x: x[0])
    # print(ts, ar)
    # input()
    if ar == buses:
        break
    else:
        ts += buses[0][1]
        n += 1
        if n % 100000 == 0:
            print(f"iteration {n:,d} (ts {ts:,d})")

print(f"earliest timestamp: {ts+1}")
