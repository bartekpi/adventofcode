from functools import reduce

with open("01.txt", "r") as f:
    input_list = [x.strip() for x in f.readlines()]

ts = int(input_list[0])
buses = [int(x) for x in input_list[1].split(",") if x != "x"]

remainders = [(b - (ts % b), b) for b in buses]

earliest = sorted(remainders, key=lambda x: x[0])[0]

print(reduce(lambda x, y: x*y, earliest))
