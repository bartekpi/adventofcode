def solve(*args):
    rounds, seq = args[0], args[1:]
    counts = dict()
    hist = []
    result = None
    i = 0

    print(f"Given the starting numbers `{','.join(map(str, seq))}`"
          f", the `{x}`th number spoken is...")
    while i < rounds:
        # print(f"Turn {i+1}")
        if i < len(seq):
            hist.append(seq[i])
            counts[seq[i]] = [i]
        else:
            last_spoken = hist[-1]
            # print(f"\tlast spoken number is {hist[-1]}. history so far: {hist[:-1]}")
            if last_spoken not in hist[:-1]:
                # print("\thas not been spoken before => next is 0")
                next_num = 0
            else:
                next_num = counts[last_spoken][-1] - counts[last_spoken][-2]
                # print("\thas been spoken before => next is {next_num}")

            hist.append(next_num)
            try:
                counts[next_num].append(i)
            except KeyError:
                counts[next_num] = [i]

        i += 1
    result = hist[-1]
    print(f"...{result}")

if __name__ == "__main__":
    solve(2020, 0, 3, 6)
    solve(2020, 1, 3, 2)
    solve(2020, 2, 1, 3)
    solve(2020, 1, 2, 3)
    solve(2020, 2, 3, 1)
    solve(2020, 3, 2, 1)
    solve(2020, 3, 1, 2)
    solve(2020, 0, 14, 6, 20, 1, 4)
