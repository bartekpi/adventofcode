from collections import defaultdict

def solve(*args):
    rounds, seq = args[0], args[1:]
    counts = defaultdict(list)
    hist = []
    histset = set()
    result = None
    i = 0
    last_spoken = None
    print(f"Given the starting numbers `{','.join(map(str, seq))}`"
          f", the `{x}`th number spoken is...")
    while i < rounds:
        # print(f"Turn {i+1}")
        if i < len(seq):
            next_num = seq[i]
        else:
            last_spoken = next_num
        
            # print(f"\tlast spoken number is {last_spoken}. history so far: {counts}")
            if len(counts[last_spoken]) < 2:
                # print("\thas not been spoken before => next is 0")
                next_num = 0
            else:
                next_num = counts[last_spoken][-1] - counts[last_spoken][-2]
                # print("\thas been spoken before => next is {next_num}")

        hist.append(next_num)
        counts[next_num].append(i)

        i += 1
    result = hist[-1]
    print(f"...{result}")

if __name__ == "__main__":
    solve(2020, 0, 3, 6)
    solve(30000000, 0, 3, 6)
    solve(30000000, 0, 14, 6, 20, 1, 4)
