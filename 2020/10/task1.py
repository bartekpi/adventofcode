from collections import Counter

with open("01.txt", "r") as f:
    input_list = [int(x.strip()) for x in f.readlines()]

diffs = []

# zero jolts
avails = [x for x in input_list if x <= 3]
if avails:
    diffs.append(min(avails))

# go through input list
for jolts in sorted(input_list):
    avails = [x-jolts for x in input_list if 0 < x - jolts <= 3]
    if avails:
        diff = min(avails)
    else:
        diff = 3
    diffs.append(diff)

counts = Counter(diffs)
x, y = counts.get(1, 0), counts.get(3, 0)
print(f"{x} * {y} = {x*y}")
