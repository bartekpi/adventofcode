from itertools import product

with open("01.txt", "r") as f:
    input_list = [int(x.strip()) for x in f.readlines()]

diffs = []
avails = []

# zero jolts
avail = [x for x in input_list if x <= 3]
if avail:
    diffs.append(min(avail))

avails.append([diffs[0]])

# go through input list
for jolts in sorted(input_list):
    avail = [(x, x-jolts) for x in input_list if 0 < x - jolts <= 3]
    if avail:
        diff = min([y for x, y in avail])
    else:
        diff = 3
    diffs.append(diff)
    if avail:
        avails.append([x for x, y in avail])

# combs = []
# for comb in product(*avails):
#     s = set(comb)
#     if s not in combs:
#         combs.append(s)
# print(f"total number of distinct arrangements: {len(combs)}")
