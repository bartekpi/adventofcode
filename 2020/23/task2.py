from time import time
from copy import copy
from collections import deque
from itertools import islice


def print_cups(cups, a):
    lst = [f"({x})" if i == a else str(x)
           for i, x in enumerate(cups)]
    return " ".join(lst)


def next_destination(active_cup, cups, cups_aside):
    destination = active_cup - 1
    if destination < 1 or destination in cups_aside:
        try:
            destination = max(filter(lambda x: x < active_cup, cups))
        except ValueError:
            destination = max(cups)
    return destination


def solve(*args, rounds=10, verbose=False):
    cups = deque(list(map(int, args[0])))
    cups.extend(range(max(cups)+1, 1_000_001))
    n = len(cups)
    r = 0
    while r < rounds:
        r += 1
        i = (r - 1) % n
        if verbose:
            print(f"\n-- move {r} --")
            print("cups:", print_cups(cups, i))

        if r == 1:
            destination = cups[0]

        cups.rotate(-3 - i - 1)
        cups_aside = list(reversed([cups.pop() for _ in range(3)]))

        destination = next_destination(cups[-1], cups, cups_aside)
        if verbose:
            print("pick up:", ", ".join(map(str, cups_aside)))
            print("destination:", destination)
            print("cups left:", cups)
        active_cup = cups[-1]
        i_dest = cups.index(destination)
        cups.rotate(-(i_dest + 1))
        cups.extend(cups_aside)
        i_active_cup = cups.index(active_cup)
        offset = (i - i_active_cup) % n
        cups.rotate(offset)

        if r % 100000 == 0:
            print(i)

    offset = cups.index(1)
    if offset:
        cups = [cups[(x + offset) % n] for x in range(n)]

    x, y = cups[1:3]
    result = x*y
    print(f"\n{x}x{y} = {result}")


if __name__ == "__main__":
    # solve("389125467", rounds=10, verbose=True)
    # solve("389125467", rounds=100)
    solve("872495136", rounds=10000000)
