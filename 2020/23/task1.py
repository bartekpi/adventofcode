def print_cups(cups, a):
    lst = [f"({x})" if i == a else str(x)
           for i, x in enumerate(cups)]
    return " ".join(lst)


def next_destination(active_cup, cups, cups_aside):
    destination = active_cup - 1
    if destination not in cups or destination in cups_aside:
        cups_available = (
            sorted([x for x in cups if x < active_cup], reverse=True)
            +
            sorted([x for x in cups if x >= active_cup], reverse=True)
        )
        for x in cups_available:
            destination = x
            if destination in cups and destination not in cups_aside:
                return destination
    return destination


def solve(*args, rounds=10, verbose=False):
    cups = list(map(int, args[0]))
    n = len(cups)
    r = 0
    rounds = 100
    while r < rounds:
        r += 1
        if verbose:
            print(f"\n-- move {r} --")

        i = (r - 1) % n
        active_cup = cups[i]
        if verbose:
            print("cups:", print_cups(cups, i))

        # cups_aside = [cups.pop((i + 1) % n) for _ in range(3)]
        cups_aside = [cups[(i + x + 1) % n] for x in range(3)]
        cups = [x for x in cups if x not in cups_aside]

        destination = next_destination(active_cup, cups, cups_aside)
        if verbose:
            print("pick up:", ", ".join(map(str, cups_aside)))
            print("destination:", destination)
            print("cups left:", cups)
        i_dest = cups.index(destination)
        cups = cups[:(1+i_dest)] + cups_aside + cups[(1+i_dest):]

        offset = cups.index(active_cup) - i
        if offset:
            cups = [cups[(x + offset) % n] for x in range(n)]

    offset = cups.index(1)
    if offset:
        cups = [cups[(x + offset) % n] for x in range(n)]
    result = "".join(map(str, cups[1:]))
    print("\nResult", result)


if __name__ == "__main__":
    solve("389125467", rounds=10, verbose=True)
    solve("389125467", rounds=100)
    solve("872495136", rounds=10)
