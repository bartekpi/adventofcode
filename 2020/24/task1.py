from collections import defaultdict, Counter


class Point():
    def __init__(self):
        self.x = 0
        self.y = 0
        self.directions = {
            "e": (1, 1),
            "w": (-1, -1),
            "se": (1, 0),
            "sw": (0, -1),
            "ne": (0, 1),
            "nw": (-1, 0),
            }
        self.black_tiles = defaultdict(bool)
        self.black_tiles[(0, 0)] = True


    def flip_tile(self, x, y):
        self.black_tiles[(x, y)] = not(self.black_tiles[(x, y)])


    def move(self, d):
        dx, dy = self.directions[d]
        self.x = self.x + dx
        self.y = self.y + dy
        self.flip_tile(self.x, self.y)


    def show(self):
        print(f"Current position: ({self.x}, {self.y})")
        print("black tiles:")
        print([x for x, y in self.black_tiles.items() if y])


    def count_black_tiles(self):
        return len([x for x, y in self.black_tiles.items() if y])


    def current_position(self):
        return (self.x, self.y)


def parse_directions(line):
    i = 0
    s = ""
    while i < len(line):
        s += line[i]
        if s in ["e", "w", "se", "sw", "ne", "nw"]:
            yield s
            s = ""
        i += 1


def solve(*args, verbose=False):
    with open(args[0], "r") as f:
        input_list = [x.strip() for x in f.readlines()]
    points = []
    for line in input_list:
        p = Point()
        if verbose:
            print(f"parsing: {line}")
        for d in parse_directions(line):
            if verbose:
                print(f"Moving to {d}")
            p.move(d)
        points.append(p.current_position())
        if verbose:
            n = p.count_black_tiles()
            print(f"Point has {n} black tiles")
            p.show()
    black_tiles = [x for x, y in Counter(points).items() if y % 2 != 0]
    print(f"Black tiles: {len(black_tiles)}")


if __name__ == "__main__":
    solve("01test.txt", verbose=False)
    solve("01.txt", verbose=False)
