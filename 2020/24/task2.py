from collections import defaultdict, Counter
from copy import deepcopy


class Point():
    def __init__(self):
        self.x = 0
        self.y = 0
        self.directions = {
            "e": (1, 1),
            "w": (-1, -1),
            "se": (1, 0),
            "sw": (0, -1),
            "ne": (0, 1),
            "nw": (-1, 0),
            }
        self.black_tiles = defaultdict(bool)
        self.black_tiles[(0, 0)] = True


    def flip_tile(self, x, y):
        self.black_tiles[(x, y)] = not(self.black_tiles[(x, y)])


    def move(self, d):
        dx, dy = self.directions[d]
        self.x = self.x + dx
        self.y = self.y + dy
        self.flip_tile(self.x, self.y)


    def show(self):
        print(f"Current position: ({self.x}, {self.y})")
        print("black tiles:")
        print([x for x, y in self.black_tiles.items() if y])


    def count_black_tiles(self):
        return len([x for x, y in self.black_tiles.items() if y])


    def current_position(self):
        return (self.x, self.y)


class Grid():


    def __init__(self):
        self.directions = {
            "e": (1, 1),
            "w": (-1, -1),
            "se": (1, 0),
            "sw": (0, -1),
            "ne": (0, 1),
            "nw": (-1, 0),
            }
        self.tiles = defaultdict(bool)
        self.grid = []

    def add_point(self, x, y):
        self.tiles[(x, y)] = not(self.tiles[(x, y)])

    
    def show(self):
        grid = self._make_grid()
        
        s = ""
        for x in grid:
            s += "".join(["X" if y else "." for y in x])
            s += "\n"
        print(s)


    def _make_grid(self):
        xs = [x for x, y in self.tiles]
        ys = [y for x, y in self.tiles]
        x_min, x_max = min(xs), max(xs)
        y_min, y_max = min(ys), max(ys)
        
        grid = []
        for x in range(x_min-1, x_max+2):
            grid.append([self.tiles[(x, y)] for y in range(y_min-1, y_max+2)])

        return grid


    def transform(self):
        grid = self._make_grid()
        tiles = dict(self.tiles.items())
        for (x, y), b in tiles.items():
            n = 0
            for dx, dy in self.directions.values():
                if tiles.get((x+dx, y+dy)):
                    n += 1
            cond = b and (n == 0 or n > 2)
            cond |= (not b) and (n == 2)
            if cond:
                self.tiles[(x, y)] = not(self.tiles[(x, y)])


    def count_black_tiles(self):
        return len([x for x, y in self.tiles.items() if y])


def parse_directions(line):
    i = 0
    s = ""
    while i < len(line):
        s += line[i]
        if s in ["e", "w", "se", "sw", "ne", "nw"]:
            yield s
            s = ""
        i += 1


def solve(*args, verbose=False):
    with open(args[0], "r") as f:
        input_list = [x.strip() for x in f.readlines()]
    points = []
    for line in input_list:
        p = Point()
        if verbose:
            print(f"parsing: {line}")
        for d in parse_directions(line):
            if verbose:
                print(f"Moving to {d}")
            p.move(d)
        points.append(p.current_position())
        if verbose:
            n = p.count_black_tiles()
            print(f"Point has {n} black tiles")
            p.show()
    black_tiles = [x for x, y in Counter(points).items() if y % 2 != 0]
    print(f"Black tiles: {len(black_tiles)}")
    g = Grid()
    for p in black_tiles:
        g.add_point(*p)

    for _ in range(100):
        g.transform()
    black_tiles = g.count_black_tiles()
    print(f"Black tiles: {black_tiles}")


if __name__ == "__main__":
    solve("01test.txt", verbose=False)
    solve("01.txt", verbose=False)
