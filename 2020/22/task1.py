def solve(*args):
    with open(args[0], "r") as f:
        deck = {}
        deck[1], deck[2] = [
            list(map(int, x[1].strip().split("\n")))
            for x in 
            map(lambda x: x.split(":"),
                f.read().split("\n\n"))
            ]
    i = 1
    ps = [1, 2]
    while deck[1] and deck[2]:
        print(f"\n-- Round {i} --")
        for p in ps:
            print(f"Player {p}'s deck:", ", ".join(map(str, deck[p])))
        plays = [deck[p].pop(0) for p in ps]

        for p in ps:
            print(f"Player {p}'s plays:", plays[p-1])
        if plays[0] > plays[1]:
            w = 1
            o = 1
        else:
            w = 2
            o = -1
        print(f"Player {w} wins the round!")
        deck[w].extend(plays[::o])
        i += 1

    print("\n== Post-game results ==")
    for p in ps:
        print(f"Player {p}'s deck:", ", ".join(map(str, deck[p])))
    
    result = sum([x*y for x, y in zip(deck[w][::-1], range(1, 1+len(deck[w])))])
    print("\nResult", result)


if __name__ == "__main__":
    solve("01test.txt")
    solve("01.txt")
