from functools import reduce

with open("01.txt", "r") as f:
    input_list = f.read()

answers = [reduce(lambda x, y: x & y, map(set, x.split()))
           for x in input_list.split("\n\n")]

print("counts:", sum(map(len, answers)))
