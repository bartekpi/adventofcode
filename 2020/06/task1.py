with open("01.txt", "r") as f:
    input_list = f.read()

answers = [(set(x.replace("\n", "").strip()))
           for x in input_list.split("\n\n")]

print("counts:", sum(map(len, answers)))
