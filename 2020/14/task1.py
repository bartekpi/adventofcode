def applymask(num, mask):
    num_bin = bin(num).split("b")[1]
    num_str = (len(mask) - len(num_bin))*"0" + num_bin
    result = ""
    for m, x in zip(mask, num_str):
        if m == "X":
            result += x
        else:
            result += m
    return int(result, 2)


def solve(*args):
    with open(args[0], "r") as f:
        input_list = [x.strip() for x in f.readlines()]

    mem = {}
    for item in input_list:
        if item.startswith("mask"):
            mask = item.split("=")[1].strip()
        if item[:3] == "mem":
            addr = int(item[4:(4+item[4:].index("]"))])
            num = int(item.split("=")[1].strip())
            mem[addr] = applymask(num, mask)
        # print(mem)
    print(f"Sum of all values is {sum(mem.values())}")


if __name__ == "__main__":
    solve("01test.txt")
    solve("01.txt")
