from itertools import product


def applymask(num, mask):
    num_bin = bin(num).split("b")[1]
    num_str = (len(mask) - len(num_bin))*"0" + num_bin
    result = ""
    for m, x in zip(mask, num_str):
        if m == "0":
            result += x
        elif m == "1":
            result += m
        else:
            result += "X"

    xs = sum([1 for x in result if x == "X"])
    result_arr = []
    for el in list(product(*[[0, 1] for _ in range(xs)])):
        res = list(result)
        for x in el:
            loc = res.index('X')
            res[loc] = str(x)
        # print("".join(res), int("".join(res), 2))
        result_arr.append(int("".join(res), 2))

    return result_arr


def solve(*args):
    with open(args[0], "r") as f:
        input_list = [x.strip() for x in f.readlines()]

    mem = {}
    for item in input_list:
        if item.startswith("mask"):
            mask = item.split("=")[1].strip()
        if item[:3] == "mem":
            addr = int(item[4:(4+item[4:].index("]"))])
            num = int(item.split("=")[1].strip())
            mems = applymask(addr, mask)
            for a in mems:
                mem[a] = num
        # print(mem)
    print(f"Sum of all values is {sum(mem.values())}")


if __name__ == "__main__":
    # solve("01test2.txt")
    solve("01.txt")
