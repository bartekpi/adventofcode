from operator import xor


input_list = []

with open("01.txt", "r") as f:
    input_list = map(lambda x: x.split(":"), f.readlines())

valid = 0

for rule, string in input_list:
    rng, c = rule.split(" ")
    pos1, pos2 = map(int, rng.split("-"))
    string = string.strip()
    if xor(string[pos1-1] == c, string[pos2-1] == c):
        valid += 1

print(f"valid passwords: {valid}")
