from collections import Counter


input_list = []

with open("01.txt", "r") as f:
    input_list = map(lambda x: x.split(":"), f.readlines())

valid = 0

for rule, string in input_list:
    rng, c = rule.split(" ")
    rng_min, rng_max = map(int, rng.split("-"))
    count = Counter(string)
    x = count.get(c, 0)
    if x >= rng_min and x <= rng_max:
        valid += 1

print(f"valid passwords: {valid}")
