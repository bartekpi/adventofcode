from functools import reduce
from copy import deepcopy


def solve(*args):
    with open(args[0], "r") as f:
        defs, my_ticket, tickets = f.read().split("\n\n")
    
    my_ticket = list(map(int, my_ticket.split(":")[1].strip().split(",")))

    rules = {}
    for i, ln in enumerate(defs.split("\n")):
        key, values = ln.split(":")
        rules[i] = {
            "name": key,
            "values": [list(map(int, x.split("-"))) for x in values.split("or")]
            }

    result = 0
    valid_tickets = []
    for ticket in tickets.strip().split("\n")[1:]:
        vals = list(map(int, ticket.strip().split(",")))
        validated_row = True
        for i, val in enumerate(vals):
            validated = []
            for k in rules:
                for min_, max_ in rules[k]["values"]:
                    if val < min_ or val > max_:
                        validated.append(False)
                    else:
                        validated.append(True)
            if not any(validated):
                result += val
                validated_row = False

        if validated_row:
            valid_tickets.append(vals)

    # print(f"Ticket scanning error rate: {result}")
    # print(f"valid tickets:\n{valid_tickets}")
    result = []
    for vals in valid_tickets:
        ticket_satisfies = []
        for i, val in enumerate(vals):
            # print(f"({i}) val={val}")
            val_satisfies = []
            for k in rules:
                validated = []
                # print(f"checking rules to be field {k}")
                for min_, max_ in rules[k]["values"]:
                    # print(f"is {val} between {min_} and {max_}?")
                    if val >= min_ and val <= max_:
                        validated.append(True)
                    else:
                        validated.append(False)
                # print(f"{any(validated)}!")
                if any(validated):
                    val_satisfies.append(k)
            ticket_satisfies.append(val_satisfies)
        result.append(ticket_satisfies)

    candidates = {}
    for i in range(len(rules)):
        candidates[i] = reduce(lambda x, y: x&y, (map(set, [x[i] for x in result])))
    
    remaining = deepcopy(rules)
    assigned = {}
    while candidates:
        k, v = sorted(candidates.items(), key=lambda x: len(x[1]))[0]
        # print(k, v)
        val = list(v & set(remaining.keys()))[0]
        assigned[k] = remaining[val]
        del remaining[val], candidates[k]
    print(f"assignments: {assigned}")

    departures = [k for k, v in assigned.items() if "departure" in v["name"]]
    print(departures)
    print([my_ticket[x] for x in departures])
    print("result is", reduce(lambda x, y: x*y, [my_ticket[x] for x in departures]))


if __name__ == "__main__":
    # solve("01test2.txt")
    solve("01.txt")
