def solve(*args):
    with open(args[0], "r") as f:
        defs, my_ticket, tickets = f.read().split("\n\n")
    
    rules = {}
    for i, ln in enumerate(defs.split("\n")):
        key, values = ln.split(":")
        rules[i] = {
            "name": key,
            "values": [list(map(int, x.split("-"))) for x in values.split("or")]
            }

    result = 0
    for ticket in tickets.strip().split("\n")[1:]:
        vals = list(map(int, ticket.strip().split(",")))
        # print(f"checking {vals}")
        for i, val in enumerate(vals):
            # print(f"\tchecking against {rules[i]}")
            validated = []
            for k in rules:
                for min_, max_ in rules[k]["values"]:
                    if val < min_ or val > max_:
                        validated.append(False)
                    else:
                        validated.append(True)
            # print(validated)
            if not any(validated):
                result += val
                # print(f"+{val}")

    print(f"Ticket scanning error rate: {result}")


if __name__ == "__main__":
    solve("01test.txt")
    solve("01.txt")
