function solve(file_name)
    numbers = map(x -> parse(Int, x), readlines(file_name));
    N = length(numbers)
    for i in 1:N
        for j in i+1:N
            for k in j+1:N
                vals = numbers[[i, j, k]]
                if sum(vals) == 2020
                    print(reduce(*, vals))
                    return nothing
                end
            end
        end
    end
end

solve("01test.txt")
solve("01.txt")
