from itertools import combinations
from functools import reduce


input_list = []

with open('01.txt', 'r') as f:
    input_list = list(map(int, f.readlines()))

for x in combinations(input_list, 3):
    if sum(x) == 2020:
        result = reduce(lambda x, y: x*y, x)
        print(f"{x[0]} x {x[1]} x {x[2]} = {result}")
        break
