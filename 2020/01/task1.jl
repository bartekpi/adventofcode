function solve(file_name)
    numbers = map(x -> parse(Int, x), readlines(file_name));

    for (i, val1) in enumerate(numbers)
        for j in i+1:length(numbers)
            val2 = numbers[j]
            if val1 + val2 == 2020
                println("$val1 * $val2 = $(val1 * val2)")
                return nothing
            end
        end
    end
end

solve("01test.txt")
solve("01.txt")
