input_list = []

with open('01.txt', 'r') as f:
    input_list = list(map(int, f.readlines()))

for i in range(len(input_list)):
    for j in range(i + 1, len(input_list)):
        if input_list[i] + input_list[j] == 2020:
            print(f"{input_list[i]} x {input_list[j]}"
                  f" = {input_list[i]*input_list[j]}")
            break
