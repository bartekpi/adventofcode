import math

with open("01.txt", "r") as f:
    input_list = [x.strip() for x in f.readlines()]

r, a = 1, 0
d = {
    "E": [1., 0.],
    "N": [0., 1.],
    "W": [-1., 0.],
    "S": [0., -1.],
}

direction = d["E"]
coords = [0, 0]

for instruction in input_list:
    # print(f"coords before: {coords}, instruction: {instruction}")
    action, x = instruction[0], int(instruction[1:])
    if action == "F":
        coords = [coords[0] + x*direction[0], coords[1] + x*direction[1]]
    elif action in list("LR"):
        a = (a + x * ((-1) ** (action == "R"))) % 360
        direction = [
            round(r*math.cos(math.radians(a)), 0),
            round(r*math.sin(math.radians(a)), 0)
            ]
    elif action in list("NSEW"):
        coords = [coords[0] + x*d[action][0], coords[1] + x*d[action][1]]
    # print(f"coords after: {coords}")
    # input()
print(f"coords after: {coords}")
print("final distance", sum(map(abs, coords)))
