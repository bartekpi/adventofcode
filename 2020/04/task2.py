import re

with open("01.txt", "r") as f:
    input_list = f.read().split("\n\n")
input_list = [x.replace("\n", " ").strip().split(" ") for x in input_list]
input_dict = [dict(map(lambda x: x.split(":"), x)) for x in input_list]

required = set(["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"])
optional = set(["cid"])


def check_rules(p):
    if len(p["hgt"]) >= 4:
        hgt = int(p["hgt"][:-2])
        hgt_unit = p["hgt"][-2:]
    else:
        return False

    rules = [
        # byr (Birth Year) - four digits; at least 1920 and at most 2002.
        re.match(r"^\d{4}$", p["byr"].strip()),
        int(p["byr"]) >= 1920,
        int(p["byr"]) <= 2002,
        # iyr (Issue Year) - four digits; at least 2010 and at most 2020.
        re.match(r"^\d{4}$", p["iyr"].strip()),
        int(p["iyr"]) >= 2010,
        int(p["iyr"]) <= 2020,
        # eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
        re.match(r"^\d{4}$", p["eyr"].strip()),
        int(p["eyr"]) >= 2020,
        int(p["eyr"]) <= 2030,
        # hgt (Height) - a number followed by either cm or in:
        #   - If cm, the number must be at least 150 and at most 193.
        #   - If in, the number must be at least 59 and at most 76.
        re.match(r"^(\d{2}in|\d{3}cm)$", p["hgt"].strip()),
        (hgt_unit == "cm" and hgt >= 150 and hgt <= 193)
        or
        (hgt_unit == "in" and hgt >= 59 and hgt <= 76),
        # hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
        len(p["hcl"]) == 7,
        re.match(r"^#[0-9a-f]{6}$", p["hcl"].strip()),
        # ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
        len(p["ecl"]) == 3,
        re.match("amb blu brn gry grn hzl oth".replace(" ", "|"),
                 p["ecl"].strip()),
        # pid (Passport ID) - a nine-digit number, including leading zeroes.
        re.match(r"^\d{9}$", p["pid"].strip()),
    ]
    return all(rules)


is_valid = 0
for passport in input_dict:
    if not required - set(passport.keys()):
        if check_rules(passport):
            is_valid += 1

print(f"valid passports: {is_valid}")
