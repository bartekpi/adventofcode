with open("01.txt", "r") as f:
    input_list = f.read().split("\n\n")
input_list = [x.replace("\n", " ").strip().split(" ") for x in input_list]
input_dict = [dict(map(lambda x: x.split(":"), x)) for x in input_list]

required = set(["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"])
optional = set(["cid"])

is_valid = 0

for passport in input_dict:
    if not required - set(passport.keys()):
        is_valid += 1

print(f"valid passports: {is_valid}")
