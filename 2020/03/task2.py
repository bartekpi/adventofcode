from functools import reduce

with open("01.txt", "r") as f:
    input_list = [list(x.strip()) for x in f.readlines()]

results = []

n_rows = len(input_list)
n_cols = max([len(x) for x in input_list])

mvmts = [
    (1, 1),
    (3, 1),
    (5, 1),
    (7, 1),
    (1, 2)
]

for mvmt_right, mvmt_down in mvmts:
    row = 0
    col = 0
    result = 0
    while row < n_rows - 1:
        row, col = row + mvmt_down, (col + mvmt_right) % n_cols

        if input_list[row][col] == '#':
            result += 1
        else:
            pass
    results.append(result)

result_product = reduce(lambda x, y: x*y, results)
print(f"all results: {results}, multiplied: {result_product}")
