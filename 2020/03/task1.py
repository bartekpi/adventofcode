with open("01.txt", "r") as f:
    input_list = [list(x.strip()) for x in f.readlines()]

result = 0
row = 0
col = 0
n_rows = len(input_list)
n_cols = max([len(x) for x in input_list])

mvmt_right = 3
mvmt_down = 1

while row < n_rows - 1:
    row, col = row + mvmt_down, (col + mvmt_right) % n_cols

    if input_list[row][col] == '#':
        result += 1
    else:
        pass

print(f"trees encountered: {result}")
