from copy import deepcopy

with open("01.txt", "r") as f:
    input_list = [x.strip() for x in f.readlines()]

instructions_base = []
for x in input_list:
    s = x.split(' ')
    instructions_base.append([s[0], int(s[1]), 0])

results = []
for j in range(len(instructions_base)):
    instructions = deepcopy(instructions_base)
    br = 0
    if instructions[j][0] == "nop":
        instructions[j][0] = "jmp"
        # print(f"Swapping {j}: nop -> jmp")
    elif instructions[j][0] == "jmp":
        instructions[j][0] = "nop"
        # print(f"Swapping {j}: jmp -> nop")
    else:
        continue
    # inputs = "\n".join([f"{x[0]} {str(x[1]):>3s}" for x in instructions])
    # print(inputs)
    acc = 0
    i = 0
    while i < len(instructions):
        ins, a, k = instructions[i]
        if k == 0:
            instructions[i][2] += 1
        else:
            br = 1
            break
        if ins == "nop":
            i += 1
        elif ins == "acc":
            acc += a
            i += 1
        elif ins == "jmp":
            i += a
    results.append([br, acc])
print("accumulator value:", *[x[1] for x in results if x[0] == 0])
