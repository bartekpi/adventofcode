with open("01.txt", "r") as f:
    input_list = [x.strip() for x in f.readlines()]

instructions = []
for x in input_list:
    s = x.split(' ')
    instructions.append([s[0], int(s[1]), 0])

acc = 0
i = 0
while i < len(instructions):
    ins, a, k = instructions[i]
    if k == 0:
        instructions[i][2] += 1
    else:
        break
    if ins == "nop":
        i += 1
    elif ins == "acc":
        acc += a
        i += 1
    elif ins == "jmp":
        i += a
print(f"finished with acc = {acc}")
