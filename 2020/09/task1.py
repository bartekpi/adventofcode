from itertools import combinations

with open("01.txt", "r") as f:
    input_list = [int(x.strip()) for x in f.readlines()]

preamble = 25

for i in range(preamble, len(input_list)):
    candidates = input_list[i-preamble:i]
    # print(i, input_list[i], candidates)
    found = 0
    for x, y in combinations(candidates, 2):
        if x + y == input_list[i]:
            found = 1
            break
    if found:
        # print(f"{x} + {y} = {input_list[i]}")
        continue
    else:
        print(f"no pair sums up to {input_list[i]}")
        break
