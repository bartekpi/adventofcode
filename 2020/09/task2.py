from itertools import combinations

with open("01.txt", "r") as f:
    input_list = [int(x.strip()) for x in f.readlines()]

preamble = 25

for i in range(preamble, len(input_list)):
    candidates = input_list[i-preamble:i]
    # print(i, input_list[i], candidates)
    found = 0
    for x, y in combinations(candidates, 2):
        if x + y == input_list[i]:
            found = 1
            break
    if found:
        # print(f"{x} + {y} = {input_list[i]}")
        continue
    else:
        s = input_list[i]
        print(f"no pair sums up to {s:,d}")
        break

print("let's find out a contiguous subset that "
      f"sums up to {s:,d}")

ln = 2
found = 0
while not found:
    for i in range(0, len(input_list)-ln):
        candidates = input_list[i:i+ln]
        if sum(candidates) == s:
            print(f"sum({candidates}) = {sum(candidates)}")
            print(f"encryption weakness is", min(candidates) + max(candidates))
            found = 1
    ln += 1
