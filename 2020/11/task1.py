from copy import deepcopy

with open("01test.txt", "r") as f:
    input_list = [list(x.strip()) for x in f.readlines()]

print("starting point:")
print("\n".join(["".join(x) for x in input_list]))

n_rows = len(input_list)
n_cols = max([len(x) for x in input_list])

rnd = 0
while True:
    rnd += 1
    input_list_pre = deepcopy(input_list)
    # let's figure out neighbours
    neibz = [[0 for y in range(len(input_list[x]))]
             for x in range(len(input_list))]
    for row in range(len(input_list)):
        for col in range(len(input_list[row])):
            if input_list[row][col] != ".":
                row_from = max(0, row-1)
                row_to = min(row+2, n_rows)
                col_from = max(0, col-1)
                col_to = min(col+2, n_cols)
                # print(f"checking neighbours at point ({row}, {col})")
                n = 0
                for i in range(row_from, row_to):
                    for j in range(col_from, col_to):
                        if (i, j) != (row, col):
                            if input_list[i][j] == "#":
                                n += 1
                                neibz[row][col] += 1
    # reseat people
    for row in range(len(input_list)):
        for col in range(len(input_list[row])):
            if input_list[row][col] != ".":
                curr_state = input_list[row][col]
                curr_neibz = neibz[row][col]
                if curr_state == "L" and curr_neibz == 0:
                    input_list[row][col] = "#"
                if curr_state == "#" and curr_neibz >= 4:
                    input_list[row][col] = "L"

    print(f"after round {rnd}")
    print("\n".join(["".join(x) for x in input_list]))
    input()
    if input_list == input_list_pre:
        break

occupied_seats = sum([sum([1 if y == "#" else 0 for y in x])
                      for x in input_list])
print(f"Occupied seats: {occupied_seats}")
