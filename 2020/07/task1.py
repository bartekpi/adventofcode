with open("01test.txt", "r") as f:
    input_list = [x.strip() for x in f.readlines()]

bags = dict()

for items in input_list:
    outer, inner = items.split("contain")
    bag_type = " ".join(outer.split(' ')[:2])
    if bag_type not in bags:
        if inner.strip() == "no other bags.":
            inner_types = []
        else:
            inner_types = [" ".join(x.strip().split(" ")[1:3])
                           for x in inner.split(",")]
        bags[bag_type] = inner_types


