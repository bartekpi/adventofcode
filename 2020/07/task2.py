with open("01.txt", "r") as f:
    input_list = [x.strip() for x in f.readlines()]

ids = []
for x in input_list:
    rng = [0, 2**7 - 1]
    for i in range(7):
        if x[i] == 'F':
            rng = [rng[0], (rng[1] + rng[0] - 1) / 2]
        else:
            rng = [((rng[1] + rng[0] + 1) / 2), rng[1]]
    row = rng[0]

    rng = [0, 2**3 - 1]
    for i in range(7, 7+3):
        if x[i] == 'L':
            rng = [rng[0], (rng[1] + rng[0] - 1) / 2]
        else:
            rng = [((rng[1] + rng[0] + 1) / 2), rng[1]]

    col = rng[0]
    seat_id = row * 8 + col
    ids.append(seat_id)
    # print(f"{x}: row {row}, columsn {col}, seat ID {seat_id}")

print(f"Highest seat ID: {max(ids):.0f}")
missing_id = set(range(int(min(ids)), int(max(ids)+1))) - set(ids)
print(f"Your seat ID: {missing_id}")
